﻿Shader "Custom/DepthGrayscale" {
	SubShader{
		Tags{ "RenderType" = "Opaque" }

		Pass{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"

		sampler2D _CameraDepthTexture;

	struct v2f {
		float4 pos : SV_POSITION;
		float4 scrPos:TEXCOORD1;
	};

	//Vertex Shader
	v2f vert(appdata_base v) {
		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.scrPos = ComputeScreenPos(o.pos);
		//for some reason, the y position of the depth texture comes out inverted
		o.scrPos.y = 1 - o.scrPos.y;
		return o;
	}
	//Convertor hsv to rgb
	float3 hsv_to_rgb(float3 HSV)
	{
		float3 RGB = HSV.z;

		float var_h = HSV.x * 6;
		float var_i = floor(var_h);   // Or ... var_i = floor( var_h )
		float var_1 = HSV.z * (1.0 - HSV.y);
		float var_2 = HSV.z * (1.0 - HSV.y * (var_h - var_i));
		float var_3 = HSV.z * (1.0 - HSV.y * (1 - (var_h - var_i)));
		if (var_i == 0) { RGB = float3(HSV.z, var_3, var_1); }
		else if (var_i == 1) { RGB = float3(var_2, HSV.z, var_1); }
		else if (var_i == 2) { RGB = float3(var_1, HSV.z, var_3); }
		else if (var_i == 3) { RGB = float3(var_1, var_2, HSV.z); }
		else if (var_i == 4) { RGB = float3(var_3, var_1, HSV.z); }
		else { RGB = float3(HSV.z, var_1, var_2); }

		return (RGB);
	}
	//Fragment Shader
	half4 frag(v2f i) : COLOR{
		float depthValue = Linear01Depth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);
	half4 depth;

	float3 hsv = float3(depthValue, 0.5, 0.5);
	float3 rgb = hsv_to_rgb(hsv);
	
	depth.r = rgb.x;
	depth.g = rgb.y;
	depth.b = rgb.z;
	//Set the rgb values
	/*
	depth.r = depthValue;
	depth.g = depthValue;
	depth.b = depthValue;
	*/

	depth.a = 1;
	return depth;
	}
		ENDCG
	}
	}
		FallBack "Diffuse"
}
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
//

//Shader "Custom/DepthGreyscale" {
//	Properties{
//		_gray("Gray Scale", Float) = 1
//	}
//
//		SubShader{
//		Cull Off ZWrite Off ZTest Always
//		Tags{ "RenderType" = "Opaque" }
//
//		Pass{
//		CGPROGRAM
//#pragma vertex vert
//#pragma fragment frag
//#include "UnityCG.cginc"
//
//#define f_05 float3(.5, .5, .5)
//#define f_10 float3(1., 1., 1.)
//
//		float _gray;
//
//	sampler2D _CameraDepthTexture;
//
//	struct v2f {
//		float4 pos : SV_POSITION;
//		float4 scrPos:TEXCOORD1;
//	};
//
//	v2f vert(appdata_base v) {
//		v2f o;
//		o.pos = UnityObjectToClipPos(v.vertex);
//		o.scrPos = ComputeScreenPos(o.pos);
//		return o;
//	}
//
//	half4 frag(v2f i) : COLOR{
//		// the value is between [0, 1] = [near plane, far plane]
//		float depth = Linear01Depth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);
//
//	if (_gray > 0.)
//		return half4(depth, depth, depth, 1.);
//
//	float3 d = f_05 + f_05 * cos(6.28318*(f_10*depth + float3(.0, .33, .67)));
//	return half4(d.r, d.g, d.b, 1.);
//	}
//		ENDCG
//	}
//	}
//		FallBack "Diffuse"
//}
//USE THIS SUBSHADER FOR A GREYSCALE DEPTH MAP
//	SubShader{
//	Tags{ "RenderType" = "Opaque" }
//
//	Pass{
//	CGPROGRAM
//	#pragma vertex vert
//	#pragma fragment frag
//	#include "UnityCG.cginc"
//
//	sampler2D _CameraDepthTexture;
//
//	struct v2f {
//		float4 pos : SV_POSITION;
//		float4 scrPos:TEXCOORD1;
//	};
//
//	//Vertex Shader
//	v2f vert(appdata_base v) {
//		v2f o;
//		o.pos = UnityObjectToClipPos(v.vertex);
//		o.scrPos = ComputeScreenPos(o.pos);
//		//for some reason, the y position of the depth texture comes out inverted
//		//o.scrPos.y = 1 - o.scrPos.y;
//		return o;
//	}
//
//	//Fragment Shader
//	half4 frag(v2f i) : COLOR{
//		float depthValue = Linear01Depth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);
//		half4 depth;
//		depth.r = depthValue;
//		depth.g = depthValue;
//		depth.b = depthValue;
//
//		depth.a = 1;
//		return depth;
//	}
//	ENDCG
//	}
//	}
//	FallBack "Diffuse"
//}

