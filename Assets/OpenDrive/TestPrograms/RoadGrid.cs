﻿using System.Collections.Generic;
using UnityEngine;

//This script generates XML for an m by n road grid and saves it as the file "road-grid.xodr"

public class RoadGrid : ProgramBaseClass
{
    //Define my ProDM vars here
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });
    VarInterval<int> connLength = new VarInterval<int>(20, 10, 50);
    VarInterval<int> numCols = new VarInterval<int>(5, 1, 7);
    VarInterval<int> numRows = new VarInterval<int>(3, 1, 4);


    public override void Test()
    {

        List<ProDM> TestVars = new List<ProDM>() { numCols, numRows, numLanes, connLength };

        CreateTestFiles("road-grid", TestVars, 5);

    }

    public override RoadElement RoadNetwork()
    {

        RoadElement roadGrid = new RoadElement(nameof(roadGrid));
        return roadGrid.buildRoadGrid(numLanes, numCols, numRows, 20);

    }


}



