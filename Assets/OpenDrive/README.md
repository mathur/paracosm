# Paracosm's OpenDRIVE output generation

## Introduction
Paracosm's programming interface can be used to output [OpenDRIVE](http://www.opendrive.org/project.html) specifications.
OpenDRIVE specifications are XML-style descriptions of the environment that an autonomous driving agent will be tested in.
Though Paracosm can generate dynamically changinig environments, OpenDRIVE's static nature restricts this capability.
Rather, a family of environments can be outputted depending on the program parameters.

## Pre-requisites
[Unity](https://store.unity.com/)
### Note
Though the OpenDRIVE specification generator does not depend on the Unity game engine, this is still (for the time being) a dependency.
This is because the rest of the Paracosm project relies on Unity for simulation.

## Structure of a Paracosm program for OpenDRIVE output
Paracosm programs that output OpenDRIVE specifications need to inherit from the `ProgramBaseClass`.
The structure of a standard OpenDRIVE output program (say `MyRoadNetwork`) is:

```cs
public class MyRoadNetwork :  ProgramBaseClass
{
	// 1. Declare test parameters of the program
	
	public override RoadElement RoadNetwork()
	{
		// 2. Create road network here
	}
	
	public override void Test()
	{
		// 3. Generate the output file(s) and specify which parameters to sample from
	}
}
```
We first declare parameters that shall be used within the program. These are useful to create parameterized environments.
For example, the length of the road, number of lanes or number of loop iterations in the program can be parameterized.
The are two types of test parameters possible with Paracosm: `VarEnum`, which enables specification of a list of values of which the test parameter can take one.
The other one is `VarInterval`, which enables specification of a min and max value and the test value generator samples between these.

In the `RoadNetwork` function, a road network is created and the final encapsulated `RoadElement` needs to be returned.
Examples are presented in the following sections.

In the `Test` function, we specify how many samples we need and the test parameters that need to be sampled.
For example, if we specify the test iterations to be 10, we get 10 OpenDRIVE specification files, each with a new sample of test parameters.

These steps shall be made more concrete in the following sections.

## Step-by-step tutorial
We present a tutorial to help you get started with using Paracosm's programming interface to generate OpenDRIVE specifications.

### Initial set-up.
Open the project in Unity (you will need to clone the entire project at https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment).
Open the scene `OpenDriveSample` (located in `Assets/Scenes`).
In the scene, click on the `ScriptPlaceHolder` object.
Then, in the inspector, you will have two scripts:
- `Visualize_XML`: the support that generates the desired OpenDRIVE network
- `StraightRoadExample`: specifies the desired road network and test
The `StraightRoadExample` should have the box by the name checked.

This image shows the Unity editor, with key features highlighted in green.
![key features](https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment/blob/0c09756f79937fb1ce323c578ef25f5feb636950/documentation/unity-interface.png "key features")

If you want to run a different road network script, first remove the `StraightRoadExample` script by right-clicking and selecting the `Remove Component` option. 
To add a new script, simply drag it over to the inspector.

Some example scripts for OpenDRIVE output are provided in https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment/tree/master/Assets/OpenDrive/Examples .

### Creating a simple straight road segments
The following example creates a straight road of variable number of lanes and length.
The output is 10 OpenDRIVE files that sample on the parameter space.

```cs
public class StraightRoadExample : ProgramBaseClass
{
    // Test variables
    // A VarEnum represents a parameter that takes a value from the elements specified in the list
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });
    // A Var Interval chooses a value between the supplied min and max
    VarInterval<int> length = new VarInterval<int>(min: 5, max: 50);

    // Create the desired road network here
    public override RoadElement RoadNetwork()
    {
        // The parameterized straight road
        RoadSegment road = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);

        // The road to be visualized needs to be returned
        return road;
    }

    // Specify the desired test here
    public override void Test()
    {
        // A list of test parameters
        List<ProDM> testVars = new List<ProDM>() { numLanes, length };

        // Create the test file(s) by sampling from test parameters for certain number of iterations >=1
        CreateTestFiles(baseFileName: "straight-road-example", testVars: testVars, iterations: 10);
    }
}
```
The output is as:
![output straight road](https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment/blob/master/documentation/straight-road.png "output straight road")

We provide the option to add trees to different sides of the road and corners of the intersections.
This line of code modifies the straight road to have trees on either side:
```cs
RoadSegment road = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);
```
The output is as:
![output trees](https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment/blob/master/documentation/road-with-trees.png "output trees")

### Adding intersections
The following example creates a cross intersection.

```cs
public class IntersectionExample : ProgramBaseClass
{
    // specify variables here
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });

    //  the desired road network here
    public override RoadElement RoadNetwork()
    {
        // make the intersection
        CrossIntersection int1 = new CrossIntersection(numLanes: numLanes);

        // return final road network to visalize
        return int1;
    }

    // specify the desired test here
    public override void Test()
    {
        // create a list of test variables
        List<ProDM> testVars = new List<ProDM>() { numLanes };

        // create the test
        CreateTestFiles(baseFileName: "intersection-example", testVars: testVars, iterations: 1);
    }
}
```
The output is as:
![output intersection](https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment/blob/master/documentation/intersection.png "output intersection")

### Making more complicated networks
The following example creates a more complicated road network.

```cs
public class ConnectedRoadsExample : ProgramBaseClass
{
    // specify variables here
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });
    VarInterval<int> length = new VarInterval<int>(val: 20, min: 5, max: 50);

    // create the desired road network here
    public override RoadElement RoadNetwork()
    {
        // make the T-intersection
        TIntersection int1 = new TIntersection(numLanes: numLanes);

        // make straight roads to connect to the Intersection
        RoadSegment cRoad1 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);
        RoadSegment cRoad2 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);
        RoadSegment cRoad3 = new RoadSegment(lengthOfRoad: length, numLanes: numLanes);

        // add straight roads to a list
        List<RoadElement> cRoadList = new List<RoadElement>
        {
            cRoad1,
            cRoad2,
            cRoad3
        };

        // make an encapsulating object of straight roads
        RoadElement cRoads = new RoadElement(cRoadList);

        // Connect the encapsulated object to T-Interesection
        // first specify the connection mapping
        Dictionary<PhysicalConnection, PhysicalConnection> connMap1 = new Dictionary<PhysicalConnection, PhysicalConnection>
        {
            { cRoad1._connections.AtIndex(1), int1._connections.AtIndex(0) },
            { cRoad2._connections.AtIndex(0), int1._connections.AtIndex(1) },
            { cRoad3._connections.AtIndex(0), int1._connections.AtIndex(2) }
        };
        // then make the connection
        RoadElement finalconnection = cRoads.ConnectTo(r2: int1, connectionMap: connMap1);

        // return final road network to visalize
        return finalconnection;
    }

    // specify the desired test here
    public override void Test()
    {
        // create a list of test variables
        List<ProDM> testVars = new List<ProDM>() { numLanes, length };

        // create the test
        CreateTestFiles(baseFileName: "connected-roads-example", testVars: testVars, iterations: 1);
    }
}
```
The output is as:
![output complex](https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment/blob/master/documentation/connected-roads.png "output complex")

We provide the option to supress pedestrian crossings and traffic signals in the intersections, although the default option is true.
This line of code modifies the T-Interesection in the above example to remove traffic signals and pedestrian crossings.
```cs
TIntersection int1 = new TIntersection(numLanes: numLanes, trafficSignal: false, pedestrianCrossing: false);
```
The output is as:
![output plain](https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment/blob/master/documentation/no-signals.png "output plain")

### Programmer conveniences
#### Making a Road Grid

We provide a built-in function that creates a road grid of size n-by-m.

```cs
public class RoadGridExample : ProgramBaseClass
{
    // specify test variables here
    VarEnum<int> numLanes = new VarEnum<int>(new List<int>() { 2, 4, 6 });
    VarInterval<int> connLength = new VarInterval<int>(20, 10, 50);
    VarInterval<int> numCols = new VarInterval<int>(5, 1, 7);
    VarInterval<int> numRows = new VarInterval<int>(3, 1, 4);

    // specify the desired road network here
    public override RoadElement RoadNetwork()
    {
        // create road element
        RoadElement roadGrid = new RoadElement(id: "roadGrid");

        // construct the road grid
        roadGrid = roadGrid.buildRoadGrid(numLanes: numLanes, numRows: numRows, numCols: numCols, lengthofconnection: 20);

        //return final road network to visalize
        return roadGrid;

    }

    // specify the desired test here
    public override void Test()
    {
        // create a list of test variables
        List<ProDM> TestVars = new List<ProDM>() { numCols, numRows, numLanes, connLength };

        // create the test
        CreateTestFiles(baseFileName: "road-grid-example", testVars: TestVars, iterations: 5);

    }
}
```
The output is as:
![output grid](https://gitlab.mpi-sws.org/mathur/ProgrammingDMEnvironment/blob/master/documentation/road-grid.png "output grid")

## Planned future additions
0. Continuous ranged parameter support
1. (later) curved road segments

## Contact
mathur@mpi-sws.org