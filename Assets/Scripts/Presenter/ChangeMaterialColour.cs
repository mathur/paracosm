﻿/*
 * This script is attached to game objects to change material colour 
 * 
 *  */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterialColour : MonoBehaviour
{
    public Material matChangeColor;

	public void ChangeColor(Color desiredColor)
    {
        // Set reference if not done already
        if (matChangeColor == null)
        {
            Debug.LogError("Material reference is not set");
        }
        else
        {
            matChangeColor = new Material(matChangeColor);
        }

        var renderers = GetComponentsInChildren<Renderer>();
        foreach (var renderer in renderers)
        {
            for (var j = 0; j < renderer.materials.Length; j++)
            {
                if (renderer.materials[j].name == "Body (Instance)")
                {
                    renderer.materials[j].color = desiredColor;
                }
            }
        }
    }
}
