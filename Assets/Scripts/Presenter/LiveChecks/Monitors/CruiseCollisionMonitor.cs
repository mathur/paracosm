﻿/*  TODO: Not yet implemented
 *  This monitor, unlike the standard collision monitor, 
 *  tracks the distance between this car and the lead car and saves it in the log
 *  */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Reactive extensions
using UniRx;
using UniRx.Triggers;

public class CruiseCollisionMonitor : ActorMonitor
{
    private Vector3 minDifference;
    private float minDist;
    private float energy = 0f;
    // The actor of interest (for almost collisions)
    public GameObject actorOfInterest;
    public string actorIdOfInterest = "car";

    private Vector3 colVelocity; // In case of collision, the collision veloctiy

    // Start is called before the first frame update
    void Start()
    {
        if (actorRef == null)
        {
            throw new System.Exception("Some essential properties of CollisionMonitor are null");
        }

        monitorType = OnlineEventType.OTHER;

        //Reactive collision check
        this.OnCollisionEnterAsObservable()
            .Subscribe(col =>
            {
                if (!col.gameObject.CompareTag("Terrain")) //A relevant collision
                {
                    colVelocity = col.relativeVelocity;
                    CollisionEvent collision_event = new CollisionEvent(owner: actorRef, suspect: actorIdOfInterest, collisionVelocity: colVelocity, energy: energy);
                    ScriptPlaceholder.GetComponent<EnvironmentProgramBaseClass>().currentTestIteration.onlineEvents.Add(collision_event);
                    ScriptPlaceholder.GetComponent<EnvironmentProgramBaseClass>().currentTestIteration.collision.Add((CollisionEvent)collision_event);
                }
            });

        //Invoke repeating save dist
        InvokeRepeating("SaveDist", 0f, 0.1f);
    }

    // Executed ever so often
    public void SaveDist()
    {
        if (actorOfInterest == null) // GameObject check done here to ensure the actor of interest has been instantiated
        {
            // Find the actor of interest
            actorOfInterest = GameObject.Find(actorIdOfInterest);
        }
        float difference = transform.position.z - actorOfInterest.transform.position.z;
        OtherEvent info = new OtherEvent(actorRef, difference);
        ScriptPlaceholder.GetComponent<EnvironmentProgramBaseClass>().currentTestIteration.onlineEvents.Add(info);
    }

    // Executed at the end of the test
    public void EndOfTest()
    {
        SaveDist();
    }

}
