﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiveActorCheck : MonoBehaviour
{
    // Reference to the ego vehicle
    public AutonomousVehicle actorRef;

    public GameObject ScriptPlaceholder; // A reference to this is required and set on attachment to g.o.
}
