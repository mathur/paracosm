﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This class contains the logic that will process road segments, including their creation and formating

public class BuildRoad : MonoBehaviour {

    //specifies directionally where to connect the roads
    public enum RoadAttachmentPoint
    {
        NORTH = 0,
        EAST = 1,
        SOUTH = 2,
        WEST = 3,
        MAX = 4
    }

    private enum RoadType
    {
        LANES_0 = 0,
        LANES_1 = 1,
        LANES_2 = 2,
        LANES_4 = 3,
        LANES_6 = 4,
        ALL = 5
    }

    //private RoadAttachmentPoint m_roadAttachmentPointIndex = RoadAttachmentPoint.NORTH;
    private float m_roadPieceSize = 8;

    // Use this for initialization
    void Start()
    {


    }

    //This function was borrowed from the TrafficSystemEditor
    //It spawns a traffic system piece and makes it a child of the traffic system
    public TrafficSystemPiece spawnRoadPiece(TrafficSystemPiece roadPiecePrefab, Vector3 position, Quaternion rotation, string name)
    {
        //actually create the piece
        TrafficSystemPiece roadPiece = Instantiate(roadPiecePrefab, position, rotation) as TrafficSystemPiece;
        //custom name for piece
        roadPiece.name = name;
        //add road piece to the traffic system parent
        roadPiece.transform.parent = TrafficSystem.Instance.transform; 

        return roadPiece;
    }

    //This function was borrowed from the TrafficSystemEditor code with one hack to account for some rotation in the intersections.
    //it takes a road piece and formats it to match the piece it is attaching to.
    public void FormatRoadSegment(TrafficSystemPiece a_currentPiece, TrafficSystemPiece a_attachToPiece, RoadAttachmentPoint a_roadAttachmentPointIndex = RoadAttachmentPoint.NORTH, bool a_isIntersection = false, bool a_useEditOffset = false, bool a_useAnchorOffset = false)
    {
        

        if (!a_currentPiece)
            return;

        if (!a_attachToPiece)
            return;

        if (a_roadAttachmentPointIndex == RoadAttachmentPoint.MAX)
            a_roadAttachmentPointIndex = RoadAttachmentPoint.NORTH;

       a_currentPiece.transform.position = a_attachToPiece.transform.position;

        //HACK (probably needs fixed)-- the prefab intersections have different defauls rotations than the straight segments
        //so if you don't account for the intersection's default rotation, it will not line up properly
        if (a_isIntersection)
        {
            a_currentPiece.transform.rotation = a_currentPiece.transform.rotation * a_attachToPiece.transform.rotation;
        }
        else
        {
            a_currentPiece.transform.rotation = a_attachToPiece.transform.rotation;
        }

        

        Vector3 pos = a_attachToPiece.transform.position;

        if (a_currentPiece.m_renderer && a_attachToPiece.m_renderer)
        {
            a_currentPiece.transform.position = a_attachToPiece.m_renderer.transform.position;
            //			EditTrafficSystemPiece.transform.rotation = a_attachToPiece.m_renderer.transform.rotation;
            pos = a_attachToPiece.m_renderer.transform.position;
        }
        

        float roadPieceSize = m_roadPieceSize;

        switch (a_roadAttachmentPointIndex)
        {
            case RoadAttachmentPoint.EAST:
                {
                    if (a_attachToPiece.m_renderer && a_currentPiece.m_renderer)
                    {
                        float anchorSize = a_attachToPiece.GetRenderBounds().extents.x;
                        if (TrafficSystem.Instance.m_swapAnchorDimensions)
                            anchorSize = a_attachToPiece.GetRenderBounds().extents.z;

                        float currentSize = a_currentPiece.GetRenderBounds().extents.x;
                        if (TrafficSystem.Instance.m_swapEditDimensions)
                            currentSize = a_currentPiece.GetRenderBounds().extents.z;

                        roadPieceSize = anchorSize + currentSize;
                    }
                    pos.x = a_attachToPiece.m_renderer.transform.position.x + roadPieceSize;
                }
                break;
            case RoadAttachmentPoint.SOUTH:
                {
                    if (a_attachToPiece.m_renderer && a_currentPiece.m_renderer)
                    {
                        float anchorSize = a_attachToPiece.GetRenderBounds().extents.z;
                        if (TrafficSystem.Instance.m_swapAnchorDimensions)
                            anchorSize = a_attachToPiece.GetRenderBounds().extents.x;

                        float currentSize = a_currentPiece.GetRenderBounds().extents.z;
                        if (TrafficSystem.Instance.m_swapEditDimensions)
                            currentSize = a_currentPiece.GetRenderBounds().extents.x;

                        roadPieceSize = anchorSize + currentSize;
                    }
                    pos.z = a_attachToPiece.m_renderer.transform.position.z - roadPieceSize;
                }
                break;
            case RoadAttachmentPoint.WEST:
                {
                    if (a_attachToPiece.m_renderer && a_currentPiece.m_renderer)
                    {
                        float anchorSize = a_attachToPiece.GetRenderBounds().extents.x;
                        if (TrafficSystem.Instance.m_swapAnchorDimensions)
                            anchorSize = a_attachToPiece.GetRenderBounds().extents.z;

                        float currentSize = a_currentPiece.GetRenderBounds().extents.x;
                        if (TrafficSystem.Instance.m_swapEditDimensions)
                            currentSize = a_currentPiece.GetRenderBounds().extents.z;

                        roadPieceSize = anchorSize + currentSize;
                    }
                    pos.x = a_attachToPiece.m_renderer.transform.position.x - roadPieceSize;
                }
                break;
            case RoadAttachmentPoint.NORTH:
                {
                    if (a_attachToPiece.m_renderer && a_currentPiece.m_renderer)
                    {
                        float anchorSize = a_attachToPiece.GetRenderBounds().extents.z;
                        if (TrafficSystem.Instance.m_swapAnchorDimensions)
                            anchorSize = a_attachToPiece.GetRenderBounds().extents.x;

                        float currentSize = a_currentPiece.GetRenderBounds().extents.z;
                        if (TrafficSystem.Instance.m_swapEditDimensions)
                            currentSize = a_currentPiece.GetRenderBounds().extents.x;

                        roadPieceSize = anchorSize + currentSize;
                    }
                    pos.z = a_attachToPiece.m_renderer.transform.position.z + roadPieceSize;
                }
                break;
        }

        Vector3 posOffset = a_currentPiece.m_posOffset;
        if (a_useAnchorOffset)
            posOffset = a_attachToPiece.m_posOffset;

        Vector3 dir = a_attachToPiece.transform.position - pos;

        if (a_attachToPiece.m_renderer)
            dir = a_attachToPiece.m_renderer.transform.position - pos;

        dir = dir.normalized;

        if (a_useEditOffset || a_useAnchorOffset)
        {
            //			if(TrafficSystem.Instance.m_swapOffsetDir)
            //			{
            //				if(TrafficSystem.Instance.m_swapOffsetSize)
            //				{
            //					posOffset.x = posOffset.z * dir.z; 
            //					posOffset.y = posOffset.y * dir.y; 
            //					posOffset.z = posOffset.x * dir.x; 
            //				}
            //				else
            //				{
            //					posOffset.x = posOffset.x * dir.z; 
            //					posOffset.y = posOffset.y * dir.y; 
            //					posOffset.z = posOffset.z * dir.x; 
            //				}
            //			}
            //			else
            //			{
            if (TrafficSystem.Instance.m_swapOffsetSize)
            {
                float x = posOffset.x;
                posOffset.x = posOffset.z;
                //posOffset.y = posOffset.y; 
                posOffset.z = x;
            }
            else
            {
                /*
                 * posOffset.x = posOffset.x; 
                posOffset.y = posOffset.y; 
                posOffset.z = posOffset.z; 
                */
            }
            //			}

            if (TrafficSystem.Instance.m_negateOffsetSize)
            {
                posOffset.x = -posOffset.x;
                posOffset.y = -posOffset.y;
                posOffset.z = -posOffset.z;
            }

            a_currentPiece.transform.position = pos + posOffset;
        }
        else
            a_currentPiece.transform.position = pos;

    }

    
}
