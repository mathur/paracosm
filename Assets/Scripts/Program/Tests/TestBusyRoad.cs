﻿/*
 * Presents a complicated scenario
 * 2 straight road segments and an intersection (with traffic)
 * 
 *  */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestBusyRoad : EnvironmentProgramBaseClass
{
    //Define my ProDM vars here
 
    VarInterval<int> numCols = 1;
    VarInterval<int> numRows = 10;

    //These are the variables to control the environment
    //They need to be defined here every time, but can be set to constant values in the Start()
    VarInterval<float> lightIntensity;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    ProDMColor color;

    int numLanes = 2;
    int roadLength = 8;
    VarEnum<Color> oneOfInvariantColor = new VarEnum<Color>(new List<Color> { Color.red, Color.blue, Color.white, Color.black});
    Color carColor;
    //This adds the visualization aspect to the datastructure.
    //In the future, it should just be behind-the-scenes and not necessary in this script
    List<TestActor> actorsToUpdate = new List<TestActor>();

    void Start() //needs to be called in the start because the setup in the Visualizer happens in the Awake()
    {
        //add the play/pause button and a button to record the car path
        PlaceHolderForVisualizer.createPlayButton();
        //initialize the ProDM and VarEnum variables
        //Do not visualize the ProDM or VarEnum variables if they are fixed values.
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 1);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);

        color = new ProDMColor(1f, 0.9f, 0.8f, 1f); //initialize the color var to a constant value instead of a variable range

        ambientIntensity = new VarInterval<float>(val: 1f, min: 0f, max: 2f);

        //initialize my ProDM and VarEnum vars here
        numRows = new VarInterval<int>(val: 8, min: 1, max: 10);
        //PlaceHolderForVisualizer.Visualize(nameof(numRows), numRows);
        numCols = new VarInterval<int>(val: 1, min: 1, max: 10);
        //PlaceHolderForVisualizer.Visualize(nameof(numCols), numCols);

        //finish the setup, including the initial visualization of the scene and the environment settings
        
        MakeView();
        MakeEnvironment();
    }


    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
       
        RoadSegment cRoad0 = new RoadSegment(roadLength, numLanes, zone1:Zoning.TREES, zone2:Zoning.RESIDENTIAL_ZONE);
        PlaceHolderForVisualizer.Visualize(cRoad0);

        RoadSegment cRoad1 = new RoadSegment(roadLength, numLanes, zone1: Zoning.RESIDENTIAL_ZONE, zone2: Zoning.TREES);
        PlaceHolderForVisualizer.Visualize(cRoad1);

        List<RoadElement> cRoadList = new List<RoadElement>();
        cRoadList.Add(cRoad0);
        cRoadList.Add(cRoad1);

        //Make an encapsulating object
        RoadElement cRoads = new RoadElement(cRoadList);
        PlaceHolderForVisualizer.Visualize(cRoads);

        Intersection int0 = new CrossIntersection(numLanes);
        int0._zone1 = Zoning.TREES;
        int0._zone2 = Zoning.TREES;
        int0._zone3 = Zoning.TREES;
        int0._zone4 = Zoning.TREES;
        PlaceHolderForVisualizer.Visualize(int0);

        
        Dictionary<PhysicalConnection, PhysicalConnection> connMap = new Dictionary<PhysicalConnection, PhysicalConnection>();
        connMap.Add(cRoad0._connections.AtIndex(0), int0._connections.AtIndex(0));
        connMap.Add(cRoad1._connections.AtIndex(1), int0._connections.AtIndex(2));

        RoadElement connectedElement0 = cRoads.ConnectTo(int0, connMap);
        PlaceHolderForVisualizer.Visualize(connectedElement0);
        PlaceHolderForVisualizer.connectRoadElements(connMap, cRoads, connectedElement0, int0, cRoad0, true);


        //add test actors

        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad0, 0f, LANE.PLUS_ONE, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_TRAINING);
        PlaceHolderForVisualizer.Visualize(autonomouscar);


        // AI cars
        oneOfInvariantColor.SetDefault();
        carColor = oneOfInvariantColor;

        //Generate traffic on cRoad0---same direction
        for (float offset = 0.30f; offset < 1f; offset += 0.30f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "same1" + offset.ToString(), cRoad0, offset, LANE.PLUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
            //Add to the list of string of actor ids to be updated
            actorsToUpdate.Add(car1);
        }

        //Generate traffic on cRoad0---opposite direction
        for (float offset = 0f; offset < 1f; offset += 0.30f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "opp1" + offset.ToString(), cRoad0, offset, LANE.MINUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
            actorsToUpdate.Add(car1);
        }

        //Generate traffic on cRoad1---same direction
        for (float offset = 0f; offset < 1f; offset += 0.30f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "same2" + offset.ToString(), cRoad1, offset, LANE.PLUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
            actorsToUpdate.Add(car1);
        }

        //Generate traffic on cRoad1---opposite direction
        for (float offset = 0f; offset < 1f; offset += 0.30f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "opp2" + offset.ToString(), cRoad1, offset, LANE.MINUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
            actorsToUpdate.Add(car1);
        }

    }

    //configure the environment settings here
    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }


    //Updating the view
    public override void UpdateViewVars(bool toDefault = false)
    {
        if (toDefault)
        {
            //Set default colour to each AI car
            oneOfInvariantColor.SetDefault();
            carColor = oneOfInvariantColor;
            foreach (var key in actorsToUpdate)
            {
                EnvironmentModel.testActorList[key].GetComponent<ChangeMaterialColour>().ChangeColor(carColor);
            }      
        }
        else
        {
            //Set random colour to each AI car
            foreach (var key in actorsToUpdate)
            {
                oneOfInvariantColor.SetRandom();
                carColor = oneOfInvariantColor;
                EnvironmentModel.testActorList[key].GetComponent<ChangeMaterialColour>().ChangeColor(carColor);
            }
        }
    }
}
