# Interesting cases for tests

* Car is trained in perfect lighting. Tests are done on different values of lighting and fog.
* Car is trained for cruise control using a red car as training. Test on green car fails.
* Car is trained for cruise control using a red car as training. When a red car comes from the other direction, our car also brakes.
* Car is trained to stop for pedestrians.
* Car is trained on a model of a Sedan, but testing is done on another model, for example an SUV?
* Behavior of the cruise control car is different on 2-lane and 4-lane roads. 
 