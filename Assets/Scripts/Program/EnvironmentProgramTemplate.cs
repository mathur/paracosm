﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentProgramTemplate : EnvironmentProgramBaseClass
{

    //Define my ProDM vars here



    //These are the variables to control the environment
    //They need to be defined here every time, but can be set to constant values in the Start()
    VarInterval<float> lightIntensity;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    ProDMColor color;

    //This adds the visualization aspect to the datastructure.
    //In the future, it should just be behind-the-scenes and not necessary in this script

    void Start() //needs to be called in the start because the setup in the Visualizer happens in the Awake()
    {
        //add the play/pause button and a button to record the car path
        //PlaceHolderForVisualizer.createPlayButton();
        //PlaceHolderForVisualizer.createRecordButton();

        //initialize the ProDM and VarEnum variables
        //Do not visualize the ProDM or VarEnum variables if they are fixed values.
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 1);
        PlaceHolderForVisualizer.Visualize(nameof(fogDensity), fogDensity, false);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
        PlaceHolderForVisualizer.Visualize(nameof(lightIntensity), lightIntensity);

        color = new ProDMColor(1f, 0.9f, 0.8f, 1f); //initialize the color var to a constant value instead of a variable range
        //color = new ProDMColor(new VarInterval(val: 1f, min: 0f, max: 1f), new VarInterval(val: 0.9f, min: 0f, max: 1f),
        //    new VarInterval(val: 0.8f, min: 0f, max: 1f), 1f);
        //PlaceHolderForVisualizer.Visualize(nameof(color), color);

        ambientIntensity = new VarInterval<float>(val: 1f, min: 0f, max: 2f);
        PlaceHolderForVisualizer.Visualize(nameof(ambientIntensity), ambientIntensity, false);

        //initialize my ProDM and VarEnum vars here




        //finish the setup, including the initial visualization of the scene and the environment settings
        MakeView();
        MakeEnvironment();
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {
        //make some road elements


        //add test actors


    }

    //configure the environment settings here
    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }

}
