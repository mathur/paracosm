﻿/*
 * Presents a complicated scenario
 * 2 straight road segments and an intersection (with traffic)
 * 
 *  */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingBusyRoad : EnvironmentProgramBaseClass
{
    //Define my ProDM vars here
 
    VarInterval<int> numCols = 1;
    VarInterval<int> numRows = 10;

    //These are the variables to control the environment
    //They need to be defined here every time, but can be set to constant values in the Start()
    VarInterval<float> lightIntensity;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    ProDMColor color;

    int numLanes = 2;
    int roadLength = 10;
    VarEnum<Color> carColor = new VarEnum<Color>(new List<Color> { Color.red, Color.blue, Color.white, Color.black});
    //This adds the visualization aspect to the datastructure.
    //In the future, it should just be behind-the-scenes and not necessary in this script

    void Start() //needs to be called in the start because the setup in the Visualizer happens in the Awake()
    {
        //add the play/pause button and a button to record the car path

        //initialize the ProDM and VarEnum variables
        //Do not visualize the ProDM or VarEnum variables if they are fixed values.
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 1);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);

        color = new ProDMColor(1f, 0.9f, 0.8f, 1f); //initialize the color var to a constant value instead of a variable range

        ambientIntensity = new VarInterval<float>(val: 1f, min: 0f, max: 2f);

        //initialize my ProDM and VarEnum vars here
        numRows = new VarInterval<int>(val: 8, min: 1, max: 10);
        //PlaceHolderForVisualizer.Visualize(nameof(numRows), numRows);
        numCols = new VarInterval<int>(val: 1, min: 1, max: 10);
        //PlaceHolderForVisualizer.Visualize(nameof(numCols), numCols);

        /*
        //Hack to move the ground to accoomodate the shift in road grid
        Transform ground = GameObject.Find("GrassQuad").transform;
        ground.position= ground.position + new Vector3(0f,0f,450f);
        ground = GameObject.Find("TerrainQuad").transform;
        ground.position = ground.position + new Vector3(0f, 0f, 450f);
        */

        //finish the setup, including the initial visualization of the scene and the environment settings
        MakeView();
        MakeEnvironment();
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {

        RoadSegment cRoad0 = new RoadSegment( roadLength, numLanes, nameof(cRoad0),Orientation.VERTICAL, zone1:Zoning.TREES, zone2:Zoning.RESIDENTIAL_ZONE);
        PlaceHolderForVisualizer.Visualize(cRoad0);

        RoadSegment cRoad1 = new RoadSegment(roadLength, numLanes, nameof(cRoad1), Orientation.VERTICAL, zone1: Zoning.RESIDENTIAL_ZONE, zone2: Zoning.TREES);
        PlaceHolderForVisualizer.Visualize(cRoad1);

        List<RoadElement> cRoadList = new List<RoadElement>();
        cRoadList.Add(cRoad0);
        cRoadList.Add(cRoad1);

        //Make an encapsulating object
        RoadElement cRoads = new RoadElement(cRoadList);
        PlaceHolderForVisualizer.Visualize(cRoads);

        Intersection int0 = new CrossIntersection(numLanes);
        int0._zone1 = Zoning.TREES;
        int0._zone2 = Zoning.TREES;
        int0._zone3 = Zoning.TREES;
        int0._zone4 = Zoning.TREES;
        PlaceHolderForVisualizer.Visualize(int0);

        
        Dictionary<PhysicalConnection, PhysicalConnection> connMap = new Dictionary<PhysicalConnection, PhysicalConnection>();
        connMap.Add(cRoad0._connections.AtIndex(0), int0._connections.AtIndex(0));
        connMap.Add(cRoad1._connections.AtIndex(1), int0._connections.AtIndex(2));

        RoadElement connectedElement0 = cRoads.ConnectTo(int0, connMap);
        PlaceHolderForVisualizer.Visualize(connectedElement0);
        PlaceHolderForVisualizer.connectRoadElements(connMap, cRoads, connectedElement0, int0, cRoad0, true);


        /*
        //we need this to connect rows to each other
        RoadElement currentRow = new RoadSegment(nameof(currentRow), 1, 2);
        RoadElement previousRow = new RoadSegment(nameof(previousRow), 1, 2);
        List<PhysicalConnection> currentTops = new List<PhysicalConnection>();
        List<PhysicalConnection> previousTops = new List<PhysicalConnection>();
        List<PhysicalConnection> currentBottoms = new List<PhysicalConnection>();
        //we need this to connect the elements in the row
        RoadElement previousCol = new RoadSegment(nameof(previousCol), 1, 2);
        PhysicalConnection previousRight = previousCol._connections.NextFree();

        for (int row = 0; row < numRows; row++)
        {

            for (int col = 0; col < numCols; col++)
            {
                Intersection int0 = new CrossIntersection(nameof(int0) + row.ToString() + col.ToString(), numLanes);
                int0._zone1 = Zoning.TREES;
                int0._zone2 = Zoning.TREES;
                int0._zone3 = Zoning.TREES;
                int0._zone4 = Zoning.TREES;
                PlaceHolderForVisualizer.Visualize(int0);
                RoadSegment cRoad0 = new RoadSegment(nameof(cRoad0) + row.ToString() + col.ToString(), 7, numLanes);
                cRoad0._zone1 = Zoning.RESIDENTIAL_ZONE;
                cRoad0._zone2 = Zoning.RESIDENTIAL_ZONE;
                RoadSegment cRoad3 = new RoadSegment(nameof(cRoad3) + row.ToString() + col.ToString(), 7, numLanes);
                cRoad3._zone1 = Zoning.RESIDENTIAL_ZONE;
                cRoad3._zone2 = Zoning.RESIDENTIAL_ZONE;
                PlaceHolderForVisualizer.Visualize(cRoad0);
                PlaceHolderForVisualizer.Visualize(cRoad3);

                List<RoadElement> cRoadList0 = new List<RoadElement>();
                cRoadList0.Add(cRoad0);
                cRoadList0.Add(cRoad3);

                //Make an encapsulating object
                RoadElement cRoads0 = new RoadElement(nameof(cRoads0) + row.ToString() + col.ToString(), cRoadList0);
                PlaceHolderForVisualizer.Visualize(cRoads0);

                //Connect the encapsulated object to Interesection
                Dictionary<PhysicalConnection, PhysicalConnection> connMap0 = new Dictionary<PhysicalConnection, PhysicalConnection>();
                connMap0.Add(cRoad0._connections.AtIndex(1), int0._connections.AtIndex(0));
                connMap0.Add(cRoad3._connections.AtIndex(1), int0._connections.AtIndex(3));

                RoadElement connectedElement0 = cRoads0.ConnectTo(nameof(connectedElement0) + row.ToString() + col.ToString(), int0, connMap0);
                PlaceHolderForVisualizer.Visualize(connectedElement0);
                PlaceHolderForVisualizer.connectRoadElements(connMap0, cRoads0, connectedElement0, int0, cRoads0, true);


                if (col != 0)
                {

                    Dictionary<PhysicalConnection, PhysicalConnection> connMap = new Dictionary<PhysicalConnection, PhysicalConnection>();
                    connMap.Add(int0._connections.AtIndex(2), previousRight);

                    RoadElement colElement = connectedElement0.ConnectTo(nameof(colElement) + row.ToString() + col.ToString(), previousCol, connMap);
                    PlaceHolderForVisualizer.Visualize(colElement);
                    PlaceHolderForVisualizer.connectRoadElements(connMap, connectedElement0, colElement, previousCol, connectedElement0, true);

                    //we need this to connect the elements in the row
                    previousCol = colElement;
                    previousRight = cRoad0._connections.AtIndex(0);
                    currentTops.Add(cRoad3._connections.AtIndex(0));
                    currentBottoms.Add(int0._connections.AtIndex(1));
                }
                else
                {
                    //we need this to connect the elements in the row
                    previousCol = connectedElement0;
                    previousRight = cRoad0._connections.AtIndex(0);
                    currentTops.Add(cRoad3._connections.AtIndex(0));
                    currentBottoms.Add(int0._connections.AtIndex(1));
                }

                if(col == numCols - 1)
                {
                    currentRow = previousCol;
                }
            }

            //now we connect the current row to the previous row
            if (row != 0)
            {

                Dictionary<PhysicalConnection, PhysicalConnection> connMap1 = new Dictionary<PhysicalConnection, PhysicalConnection>();
                connMap1.Add(currentBottoms[0], previousTops[0]);

                RoadElement rowElement = currentRow.ConnectTo(nameof(rowElement) + row.ToString(), previousRow, connMap1);
                PlaceHolderForVisualizer.Visualize(rowElement);
                PlaceHolderForVisualizer.connectRoadElements(connMap1, currentRow, rowElement, previousRow, currentRow);


                //we need this to connect the elements in the row
                previousRow = rowElement;
                previousTops.Clear();
                foreach (PhysicalConnection conn in currentTops)
                {
                    previousTops.Add(conn);
                }
                currentTops.Clear();
                currentBottoms.Clear();
            }
            else
            {
                //we need this to connect the elements in the row
                previousRow = currentRow;
                previousTops.Clear();
                foreach (PhysicalConnection conn in currentTops)
                {
                    previousTops.Add(conn);
                }
                currentTops.Clear();
                currentBottoms.Clear();
            }
        }
        */
        //add test actors

        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad0, 0f, LANE.PLUS_ONE, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_TRAINING);
        PlaceHolderForVisualizer.Visualize(autonomouscar);

        //Generate traffic on cRoad0---same direction
        for (float offset = 0.20f; offset < 1f; offset += 0.20f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "same1" + offset.ToString(), cRoad0, offset, LANE.PLUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
        }

        //Generate traffic on cRoad0---opposite direction
        for (float offset = 0f; offset < 1f; offset += 0.20f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "opp1" + offset.ToString(), cRoad0, offset, LANE.MINUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
        }

        //Generate traffic on cRoad1---same direction
        for (float offset = 0f; offset < 1f; offset += 0.20f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "same2" + offset.ToString(), cRoad1, offset, LANE.PLUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
        }

        //Generate traffic on cRoad1---opposite direction
        for (float offset = 0f; offset < 1f; offset += 0.20f)
        {
            AIVehicle car1 = new AIVehicle(nameof(car1) + "opp2" + offset.ToString(), cRoad1, offset, LANE.MINUS_ONE, carColor);
            PlaceHolderForVisualizer.Visualize(car1);
        }

    }

    //configure the environment settings here
    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}
