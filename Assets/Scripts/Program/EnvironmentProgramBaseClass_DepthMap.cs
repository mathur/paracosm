﻿//This file contains the base class for programs
using System.Collections;
using System.Collections.Generic;
using System.IO; //For file operations
using System.Linq;
using UnityEngine;

public class EnvironmentProgramBaseClass_DepthMap : MonoBehaviour
{

    public Visualizer_withDepthMap PlaceHolderForVisualizer;
    private StreamWriter testLog;

    bool isAutomatedTest = false; //Is the test automated or online/hand-on?

    //Structures to hold test information that is later printed out
    TestIteration currentTestIteration;
    AggregatedLogIterative currentTestIterative;
    AggregatedLogTemporal currentTestTemporal;
    AggregatedLogStandard currentTest;

    //Set reference to the Visualizer
    private void Awake()
    {
        if (PlaceHolderForVisualizer == null)
        {
            //If not set in inspector, set PlaceHolderForVisualizer
            PlaceHolderForVisualizer = GetComponent<Visualizer_withDepthMap>();
        }

    }

    public void Train(List<VarInterval<float>> invariants = null)
    {
        //Set invariants in a public static structure
        EnvironmentModel.invariants = invariants;
        makeView();
        environmentSettings();
    }

    //Simple test = simple run on the environment program
    public void Test(float timeCutOff = 1000f)
    {
        IEnumerator coroutine = StandardTest(timeCutOff);
        StartCoroutine(coroutine);
    }

    // A standard test, that ends after cutoff seconds are elapsed
    IEnumerator StandardTest(float cutoff)
    {
        isAutomatedTest = true;

        //Create log file
        CreateTestLogFile();

        makeView();
        environmentSettings();

        yield return new WaitForSeconds(cutoff);

        //Test complete due to cut off
        currentTest = new AggregatedLogStandard(duration: cutoff, time: System.DateTime.Now.ToLongTimeString());

        //This marks the end of one iteration of the test
        CalculateAndSetTestStatistics();
        WriteStructureToTextLog();

        PlaceHolderForVisualizer.deleteView();
        Application.Quit();
    }

    public void TestIterative(List<VarInterval<float>> rangedFloats, int iterations = 10, float singleTestTime = 10f,
        AutomatedVarSetMethod setVar = AutomatedVarSetMethod.LOW_DISCREPANCY, int repetitionsPerIteration = 1)
    {
        isAutomatedTest = true;

        //Set other test var to null
        currentTestTemporal = null;
        currentTest = null;

        //Make coroutine of the iterated test
        IEnumerator coroutine = AutomatedTest(rangedFloats, iterations, singleTestTime, setVar, repetitionsPerIteration);
        StartCoroutine(coroutine);
    }

    //Change settings after time (single iteration)
    //List of List of ProDMFloats rangedFloats, List float for time
    public void TestTemporal(List<List<VarInterval<float>>> rangedFloats, List<List<float>> targetVals = null, List<float> times = null, float timeCutOff = 15f)
    {
        isAutomatedTest = true;


        //Check on the list of time
        if (times == null)
        {
            //Build the list by equally dividing time
            var divs = rangedFloats.Count;
            var interval = timeCutOff / divs;
            times = new List<float>();

            for (var i = 0; i < rangedFloats.Count; i++)
            {
                times.Add(interval);
            }
        }
        else
        {
            //Duration should be more than time of temporal disturbance
            var overallTime = 0f;
            foreach (var time in times)
            {
                //Get total time for disturbances
                overallTime += time;
            }
            if (timeCutOff <= overallTime)
            {
                throw new System.Exception("Duration should be more than time of temporal disturbance.");
            }
        }

        //Create log file
        CreateTestLogFile();

        //Make the environment
        makeView();
        environmentSettings();

        //Set other test vars to null
        currentTestIterative = null;
        currentTestIteration = null;
        currentTest = null;

        IEnumerator coroutine = AutomatedTestTemporal(rangedFloats, targetVals, times, timeCutOff);
        StartCoroutine(coroutine);
    }

    // Needs to be described 
    public virtual void makeView()
    {
        throw new System.Exception("Cannot call this funciton");
    }

    //Needs to be set
    public virtual void environmentSettings()
    {
        throw new System.Exception("Cannot call this funciton");
    }

    // Test several iterations over ranged values
    IEnumerator AutomatedTest(List<VarInterval<float>> rangedFloats, int iterations, float singleTestTime, AutomatedVarSetMethod setVar, int repetitions)
    {
        //Create log file
        CreateTestLogFile();

        //Create TestLog structure
        currentTestIterative = new AggregatedLogIterative(setVar, iterations, singleTestTime, repetitions, rangedFloats);

        List<List<float>> rangedFloatssLowDiscrepancy = new List<List<float>>(rangedFloats.Count);

        //Calculate values for all rangedFloats over all iterations
        if (setVar == AutomatedVarSetMethod.LOW_DISCREPANCY)
        {
            foreach (var rangedFloat in rangedFloats)
            {
                rangedFloatssLowDiscrepancy.Add(rangedFloat.CalculateLowDiscrepancy(iterations));
            }
        }


        // Outer loop for several iterations of the test (over different values of the ranged variables)
        for (var i = 0; i < iterations; i++)
        {
            for (int repetitionCounter = 1; repetitionCounter <= repetitions; repetitionCounter++)
            {
                currentTestIteration = new TestIteration(iterationNum: i + 1, time: System.DateTime.Now.ToLongTimeString(),
                    repetitionNumber: repetitionCounter);

                for (var j = 0; j < rangedFloats.Count; j++)
                {
                    var elemFloat = rangedFloats[j];
                    if (setVar == AutomatedVarSetMethod.RANDOM)
                        //Set a new random value
                        elemFloat.SetRandom();

                    else if (setVar == AutomatedVarSetMethod.LOW_DISCREPANCY)
                    {
                        //Set values calculated before 
                        elemFloat.ChangeValue(rangedFloatssLowDiscrepancy[j][i]);
                    }
                    else
                        throw new System.Exception("Unknown ranged (ProDM) variable set method");

                    //Create a snapshot to be saved in custom structure
                    VarInterval<float> snapshot = new VarInterval<float>(elemFloat);
                    float temp = snapshot._val;
                    currentTestIteration.rangedValues.Add(temp);
                }

                makeView();
                environmentSettings();

                yield return new WaitForSeconds(singleTestTime);

                //This marks the end of one iteration of the test
                CalculateAndSetTestStatistics();

                //Delete view and add this run to the log structure
                PlaceHolderForVisualizer.deleteView();
                currentTestIterative.testIterations.Add(currentTestIteration);
            }
            if (i == iterations - 1)
            {
                WriteStructureToTextLog();
                Application.Quit(); //Quit the application if all iterations are complete
            }
        }

    }

    //Set environment variables at some time 
    IEnumerator AutomatedTestTemporal(List<List<VarInterval<float>>> rangedFloats, List<List<float>> targetVals, List<float> times,
        float testDuration)
    {
        // Make an aggregated log for this temporal test
        currentTestTemporal = new AggregatedLogTemporal(valBefore: rangedFloats, valAfter: targetVals, timeToDisturbance: times,
            duration: testDuration, time: System.DateTime.Now.ToLongTimeString());

        var totalTimeSoFar = 0f;
        // For each time in 
        for (int timeCounter = 0; timeCounter < times.Count; timeCounter++)
        {
            var time = times[timeCounter];
            yield return new WaitForSeconds(time);
            totalTimeSoFar += time;
            for (int i = 0; i < rangedFloats[timeCounter].Count; i++)
            {
                VarInterval<float> elem = rangedFloats[timeCounter][i];
                if (targetVals == null)
                {
                    elem.SetRandom();
                }
                else
                {
                    elem = (float) targetVals[timeCounter][i];
                }
            }
            //Set the environment settings
            environmentSettings();
        }

        yield return new WaitForSeconds(testDuration - totalTimeSoFar);
        //End of simulation

        //Calculate aggregated info
        CalculateAndSetTestStatistics();

        WriteStructureToTextLog();
        PlaceHolderForVisualizer.deleteView();
        Application.Quit(); //Quit the application if all iterations are complete
    }

    private void CreateTestLogFile()
    {
        testLog = new StreamWriter("Test-" + System.DateTime.Now.ToString("yyyy-dd-M--HH-mm") + ".txt");
    }

    private void WriteToTestLog(string stuffToWrite)
    {
        testLog.WriteLine(stuffToWrite);
    }

    void WriteStructureToTextLog()
    {
        if (currentTestIterative != null)
            testLog.WriteLine(JsonUtility.ToJson(currentTestIterative, prettyPrint: true));
        else if (currentTestTemporal != null)
            testLog.WriteLine(JsonUtility.ToJson(currentTestTemporal, prettyPrint: true));
        else if (currentTest != null)
            testLog.WriteLine(JsonUtility.ToJson(currentTest, prettyPrint: true));
    }

    //Called via broadcast from OnlineCheckers
    void CollisionDetected(CollisionEvent col)
    {
        if (isAutomatedTest)
        {
            if (currentTestIteration != null)
                currentTestIteration.onlineEvents.Add(col);
            else if (currentTestTemporal != null)
                currentTestTemporal.onlineEvents.Add(col);
            else if (currentTest != null)
                currentTest.onlineEvents.Add(col);
        }
    }
    void InactivityDetected(InactivityEvent inactive)
    {
        if (isAutomatedTest)
        {
            if (currentTestIteration != null)
                currentTestIteration.onlineEvents.Add(inactive);
            else if (currentTestTemporal != null)
                currentTestTemporal.onlineEvents.Add(inactive);
            else if (currentTest != null)
                currentTest.onlineEvents.Add(inactive);
        }
    }

    // Calculate statistics for this iteration of automated test
    void CalculateAndSetTestStatistics()
    {
        if (currentTestIteration != null)
        {
            //average speed calculation
            currentTestIteration.averageSpeed = PlaceHolderForVisualizer.egoCarRef._liveSpeed.Average();

            //distance travelled calculation

            Vector3 prevPos = PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTestIteration.distanceCovered = 0f;
            foreach (var pos in PlaceHolderForVisualizer.egoCarRef._livePos)
            {
                currentTestIteration.distanceCovered += ((prevPos - pos).magnitude);
                prevPos = pos;
            }

            // displacement calculation
            currentTestIteration.displacementVector = PlaceHolderForVisualizer.egoCarRef._livePos[PlaceHolderForVisualizer.egoCarRef._livePos.Count - 1] - PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTestIteration.displacement = currentTestIteration.displacementVector.magnitude;
        }
        else if (currentTestTemporal != null)
        {
            currentTestTemporal.averageSpeed = PlaceHolderForVisualizer.egoCarRef._liveSpeed.Average();
            Vector3 prevPos = PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTestTemporal.distanceCovered = 0f;
            foreach (var pos in PlaceHolderForVisualizer.egoCarRef._livePos)
            {
                currentTestTemporal.distanceCovered += ((prevPos - pos).magnitude);
                prevPos = pos;
            }

            // displacement calculation
            currentTestTemporal.displacementVector = PlaceHolderForVisualizer.egoCarRef._livePos[PlaceHolderForVisualizer.egoCarRef._livePos.Count - 1] - PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTestTemporal.displacement = currentTestTemporal.displacementVector.magnitude;
        }
        else if (currentTest != null)
        {
            currentTest.averageSpeed = PlaceHolderForVisualizer.egoCarRef._liveSpeed.Average();
            Vector3 prevPos = PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTest.distanceCovered = 0f;
            foreach (var pos in PlaceHolderForVisualizer.egoCarRef._livePos)
            {
                currentTest.distanceCovered += ((prevPos - pos).magnitude);
                prevPos = pos;
            }

            // displacement calculation
            currentTest.displacementVector = PlaceHolderForVisualizer.egoCarRef._livePos[PlaceHolderForVisualizer.egoCarRef._livePos.Count - 1] - PlaceHolderForVisualizer.egoCarRef._livePos[0];
            currentTest.displacement = currentTest.displacementVector.magnitude;
        }
    }

    void OnApplicationQuit()
    {
        if (testLog != null)
            testLog.Close();
    }

}
