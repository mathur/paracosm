﻿//Presents a grid world

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo2 : EnvironmentProgramBaseClass {
    //Define my ProDM vars here
    int numLanes = 2;
    VarInterval<int> numCols = 10;
    VarInterval<int> numRows = 10;

    //These are the variables to control the environment
    //They need to be defined here every time, but can be set to constant values in the Start()
    VarInterval<float> lightIntensity;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;
    ProDMColor color;

    //This adds the visualization aspect to the datastructure.
    //In the future, it should just be behind-the-scenes and not necessary in this script

    void Start() //needs to be called in the start because the setup in the Visualizer happens in the Awake()
    {
        //add the play/pause button and a button to record the car path
        //PlaceHolderForVisualizer.createPlayButton();
        //PlaceHolderForVisualizer.createRecordButton();

        //initialize the ProDM and VarEnum variables
        //Do not visualize the ProDM or VarEnum variables if they are fixed values.
        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 1);
        PlaceHolderForVisualizer.Visualize(nameof(fogDensity), fogDensity, false);

        lightIntensity = new VarInterval<float>(val: 1, min: 0, max: 10);
        PlaceHolderForVisualizer.Visualize(nameof(lightIntensity), lightIntensity);

        color = new ProDMColor(1f, 0.9f, 0.8f, 1f); //initialize the color var to a constant value instead of a variable range
        //color = new ProDMColor(new VarInterval(val: 1f, min: 0f, max: 1f), new VarInterval(val: 0.9f, min: 0f, max: 1f),
        //    new VarInterval(val: 0.8f, min: 0f, max: 1f), 1f);
        //PlaceHolderForVisualizer.Visualize(nameof(color), color);

        ambientIntensity = new VarInterval<float>(val: 1f, min: 0f, max: 2f);
        PlaceHolderForVisualizer.Visualize(nameof(ambientIntensity), ambientIntensity, false);

        //initialize my ProDM and VarEnum vars here
        numRows = new VarInterval<int>(val: 5, min: 1, max: 10);
        PlaceHolderForVisualizer.Visualize(nameof(numRows), numRows);
        numCols = new VarInterval<int>(val: 5, min: 1, max: 10);
        PlaceHolderForVisualizer.Visualize(nameof(numCols), numCols);



        //finish the setup, including the initial visualization of the scene and the environment settings
        MakeView();
        MakeEnvironment();
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {

        //we need this to connect rows to each other
        RoadElement currentRow = new RoadSegment(1, 2, nameof(currentRow));
        RoadElement previousRow = new RoadSegment(1, 2, nameof(previousRow));
        List<PhysicalConnection> currentTops = new List<PhysicalConnection>();
        List<PhysicalConnection> previousTops = new List<PhysicalConnection>();
        List<PhysicalConnection> currentBottoms = new List<PhysicalConnection>();
        //we need this to connect the elements in the row
        RoadElement previousCol = new RoadSegment(1, 2, nameof(previousCol));
        PhysicalConnection previousRight = previousCol._connections.NextFree();

        for (int row = 0; row < numRows; row++)
        {

            for (int col = 0; col < numCols; col++)
            {
                Intersection int0 = new CrossIntersection(numLanes, nameof(int0) + row.ToString() + col.ToString());
                int0._zone1 = Zoning.TREES;
                int0._zone2 = Zoning.TREES;
                int0._zone3 = Zoning.TREES;
                int0._zone4 = Zoning.TREES;
                PlaceHolderForVisualizer.Visualize(int0);
                RoadSegment cRoad0 = new RoadSegment(7, numLanes, nameof(cRoad0) + row.ToString() + col.ToString());
                cRoad0._zone1 = Zoning.COMMERCIAL_ZONE;
                cRoad0._zone2 = Zoning.RESIDENTIAL_ZONE;
                RoadSegment cRoad3 = new RoadSegment(7, numLanes, nameof(cRoad3) + row.ToString() + col.ToString());
                cRoad3._zone1 = Zoning.COMMERCIAL_ZONE;
                cRoad3._zone2 = Zoning.RESIDENTIAL_ZONE;
                PlaceHolderForVisualizer.Visualize(cRoad0);
                PlaceHolderForVisualizer.Visualize(cRoad3);

                List<RoadElement> cRoadList0 = new List<RoadElement>();
                cRoadList0.Add(cRoad0);
                cRoadList0.Add(cRoad3);

                //Make an encapsulating object
                RoadElement cRoads0 = new RoadElement(cRoadList0, nameof(cRoads0) + row.ToString() + col.ToString());
                PlaceHolderForVisualizer.Visualize(cRoads0);

                //Connect the encapsulated object to Interesection
                Dictionary<PhysicalConnection, PhysicalConnection> connMap0 = new Dictionary<PhysicalConnection, PhysicalConnection>();
                connMap0.Add(cRoad0._connections.AtIndex(1), int0._connections.AtIndex(0));
                connMap0.Add(cRoad3._connections.AtIndex(1), int0._connections.AtIndex(3));

                RoadElement connectedElement0 = cRoads0.ConnectTo(int0, connMap0, nameof(connectedElement0) + row.ToString() + col.ToString());
                PlaceHolderForVisualizer.Visualize(connectedElement0);
                PlaceHolderForVisualizer.connectRoadElements(connMap0, cRoads0, connectedElement0, int0, cRoads0, true);


                if (col != 0)
                {

                    Dictionary<PhysicalConnection, PhysicalConnection> connMap = new Dictionary<PhysicalConnection, PhysicalConnection>();
                    connMap.Add(int0._connections.AtIndex(2), previousRight);

                    RoadElement colElement = connectedElement0.ConnectTo(previousCol, connMap, nameof(colElement) + row.ToString() + col.ToString());
                    PlaceHolderForVisualizer.Visualize(colElement);
                    PlaceHolderForVisualizer.connectRoadElements(connMap, connectedElement0, colElement, previousCol, connectedElement0, true);

                    //we need this to connect the elements in the row
                    previousCol = colElement;
                    previousRight = cRoad0._connections.AtIndex(0);
                    currentTops.Add(cRoad3._connections.AtIndex(0));
                    currentBottoms.Add(int0._connections.AtIndex(1));
                }
                else
                {
                    //we need this to connect the elements in the row
                    previousCol = connectedElement0;
                    previousRight = cRoad0._connections.AtIndex(0);
                    currentTops.Add(cRoad3._connections.AtIndex(0));
                    currentBottoms.Add(int0._connections.AtIndex(1));
                }

                if(col == numCols - 1)
                {
                    currentRow = previousCol;
                }
            }

            //now we connect the current row to the previous row
            if (row != 0)
            {

                Dictionary<PhysicalConnection, PhysicalConnection> connMap1 = new Dictionary<PhysicalConnection, PhysicalConnection>();
                connMap1.Add(currentBottoms[0], previousTops[0]);

                RoadElement rowElement = currentRow.ConnectTo(previousRow, connMap1, nameof(rowElement) + row.ToString());
                PlaceHolderForVisualizer.Visualize(rowElement);
                PlaceHolderForVisualizer.connectRoadElements(connMap1, currentRow, rowElement, previousRow, currentRow);


                //we need this to connect the elements in the row
                previousRow = rowElement;
                previousTops.Clear();
                foreach (PhysicalConnection conn in currentTops)
                {
                    previousTops.Add(conn);
                }
                currentTops.Clear();
                currentBottoms.Clear();
            }
            else
            {
                //we need this to connect the elements in the row
                previousRow = currentRow;
                previousTops.Clear();
                foreach (PhysicalConnection conn in currentTops)
                {
                    previousTops.Add(conn);
                }
                currentTops.Clear();
                currentBottoms.Clear();
            }
        }
        //add test actors


    }

    //configure the environment settings here
    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: lightIntensity, direction: Vector3.down, color: color, ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}
