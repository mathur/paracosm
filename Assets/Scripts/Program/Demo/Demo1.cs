﻿/*
 * A demo with
 * 
 * 
 *  */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo1 : EnvironmentProgramBaseClass {

    //Add the ProDM vars here
    VarInterval<int> roadLength;
    VarEnum<int> numLanes;
    VarInterval<float> ambientIntensity;
    VarInterval<float> fogDensity;


    void Start() //needs to be called in the start because the setup in the RoadElementVisualization happens in the Awake()
    {
        PlaceHolderForVisualizer.createPlayButton();
        PlaceHolderForVisualizer.createRecordButton();

        fogDensity = new VarInterval<float>(val: 0, min: 0, max: 0.3f, desc:"FOG_DENSITY");
        PlaceHolderForVisualizer.Visualize(nameof(fogDensity), fogDensity, false);

        ambientIntensity = new VarInterval<float>(val: 1f, min: 0f, max: 2.5f, desc: "AMBIENT_LIGHT_INTENSITY");
        PlaceHolderForVisualizer.Visualize(nameof(ambientIntensity), ambientIntensity, false);

        //define the ProDM variables
        roadLength = new VarInterval<int>(val: 100, changeable: true, min: 20, max: 200);
        PlaceHolderForVisualizer.Visualize(nameof(roadLength), roadLength);

        List<int> numLanesOptions = new List<int> { 2, 4, 6 };

        numLanes = new VarEnum<int>(numLanesOptions, 0);
        PlaceHolderForVisualizer.Visualize(nameof(numLanes), numLanes);

        Test();
    }

    //create the visualization of the road network
    //NOTE: The code to make the road network should go here.
    public override void MakeView()
    {

        RoadSegment cRoad1 = new RoadSegment(roadLength, numLanes, nameof(cRoad1), Orientation.VERTICAL);
        cRoad1._zone1 = Zoning.TREES;
        cRoad1._zone2 = Zoning.TREES;
        PlaceHolderForVisualizer.Visualize(cRoad1);

        //add a car
        AIVehicle car = new AIVehicle(nameof(car), cRoad1, 0.12f, LANE.PLUS_ONE, bodyColor: Color.red);
        PlaceHolderForVisualizer.Visualize(car);

        AutonomousVehicle autonomouscar = new AutonomousVehicle(nameof(autonomouscar), cRoad1, 0.1f, LANE.PLUS_ONE, monitor: "CollisionMonitor", type: TestActorType.EGO_VEHICLE_TRAINING);
        PlaceHolderForVisualizer.Visualize(autonomouscar);

    }


    public override void MakeEnvironment()
    {
        //Environment settings
        LightSettings light = new LightSettings(intensity: new VarInterval<float>(1f), direction: Vector3.down, color: new Color(1f,0.9f,0.8f), ambientIntensity: ambientIntensity);
        FogSettings fog = new FogSettings(density: fogDensity);

        EnvironmentSettings env = new EnvironmentSettings(light, fog);
        PlaceHolderForVisualizer.Visualize(env);
    }
}
