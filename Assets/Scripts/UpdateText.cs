﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This class updates the text labels in the UI

public class UpdateText : MonoBehaviour
{
    public Text TextLaneNum;
    public Text TextRoadLength;
    public ValueController PlaceHolderForValueControllerFunction;

    public void UpdateTextOnLabels()
    {
        //set text label for each slider, updated with current value
        TextLaneNum.text = "Number of lanes: " + PlaceHolderForValueControllerFunction.GetNumLanes();
        TextRoadLength.text = "Length of road: " + PlaceHolderForValueControllerFunction.GetRoadLength();
    }
}
