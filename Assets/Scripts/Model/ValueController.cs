﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

//This class configures and reads the slider values for the direct manipulation controls.

public class ValueController : MonoBehaviour
{
    //sliderNumLanes variables
    private int maxSliderValNumLanes;
    public GameObject PlaceHolderForLaneChange;

    public Slider SliderNumLanes;
    public ReactiveProperty<int> numLanes;
    private int numLanesIndex = 0;

    //sliderRoadLength variables
    private int maxSliderValRoadLength;
    private int minSliderValRoadLength;
    public GameObject PlaceHolderForRoadLength;

    public Slider SliderRoadLength;
    public int minRoadLength;
    public int maxRoadLength;
    public ReactiveProperty<int> roadLength;

    //dropdownIntersectionSelect variables
    public Dropdown dropdownIntersectionSelect;
    private string intersectionType;

    //refresh function
    public RefreshAndRecreateRoadSegment PlaceHolderForRefreshFunction;

    private void Awake()
    {
        //Used to set up initial variables/properties and UI elements

        //SliderNumLanes setup
        maxSliderValNumLanes = PlaceHolderForLaneChange.GetComponent<RefreshAndRecreateRoadSegment>().roadPrefabs.Count - 1;
        SliderNumLanes.maxValue = maxSliderValNumLanes;
        SliderNumLanes.minValue = 0;

        //SliderRoadLength setup
        maxSliderValRoadLength = maxRoadLength;
        minSliderValRoadLength = minRoadLength;
        SliderRoadLength.maxValue = maxSliderValRoadLength;
        SliderRoadLength.minValue = minSliderValRoadLength;
        SliderRoadLength.value = minSliderValRoadLength;

        //Set value of numLanes and  roadLength according change in slider values
        numLanes = SliderNumLanes.OnValueChangedAsObservable()
            .Select(
            x => SliderValToNumLanes(int.Parse(x.ToString()))
            ).ToReactiveProperty();

        roadLength = SliderRoadLength.OnValueChangedAsObservable()
            .Select(x =>
            {
                int val = int.Parse(x.ToString());
                return val;
            }).ToReactiveProperty();


        //set up intersection type select
        //build dropdown menu based on the road types of each intersection type prefab (Probably should be done differently)
        dropdownIntersectionSelect.options.Clear();
        dropdownIntersectionSelect.options.Add(new Dropdown.OptionData(PlaceHolderForRefreshFunction.tIntersectionPrefabs[0].m_roadPieceType.ToString()));
        dropdownIntersectionSelect.options.Add(new Dropdown.OptionData(PlaceHolderForRefreshFunction.xIntersectionPrefabs[0].m_roadPieceType.ToString()));
        dropdownIntersectionSelect.options.Add(new Dropdown.OptionData(PlaceHolderForRefreshFunction.roundaboutPrefabs[0].m_roadPieceType.ToString()));
        dropdownIntersectionSelect.value = 0;
        setIntersectionType();
        dropdownIntersectionSelect.onValueChanged.AddListener(delegate { setIntersectionType(); });

    }

    //sets intersection type
    void setIntersectionType()
    {
        //get the value from the dropdown
        int m_selectedValue = dropdownIntersectionSelect.value;
        intersectionType = dropdownIntersectionSelect.options[m_selectedValue].text;

        //if there is an intersection in the road network, refresh the view
        if (PlaceHolderForRefreshFunction.includeIntersection)
        {
            PlaceHolderForRefreshFunction.Refresh();
        }
    }

    //converts slider value to numLanes
    int SliderValToNumLanes(int x)
    {
        int val;
        switch (x)
        {
            case 0:
                val = 1;
                break;
            case 1:
                val = 2;
                break;
            case 2:
                val = 4;
                break;
            case 3:
                val = 6;
                break;
            default:
                val = -1;
                break;
        }
        return val;
    }
    //returns the number of lanes according to the numLanesIndex value
    public int GetNumLanes()
    {
        return int.Parse(numLanes.ToString());
    }

    //returns the index of the road piece according to the slider value
    public int GetNumLanesIndex()
    {
        return numLanesIndex;
    }

    //returns the road length according to the slider value
    public int GetRoadLength()
    {
        return int.Parse(roadLength.ToString());
    }

    //returns a string with the intersection type from the dropdown
    public string GetIntersectionType()
    {
        return intersectionType;
    }

}
