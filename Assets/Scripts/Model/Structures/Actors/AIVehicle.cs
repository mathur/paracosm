﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Vehicles that have some simple AI control
public class AIVehicle : TestActor
{
    //The string name of the model to use
    public string _model;
    //The goal position where the actor tries to reach
    public ProDMVector3 _goalPos;
    //The contiguous road where the actor would spawn
    public RoadSegment _road;
    //The placement along the contiguous road where the actor would spawn
    public float _normalizedDistance;
    //The lane where the actor will spawn
    public LANE _laneNum;
    //Color of the car
    public Color _bodyColor;
    //specify if car color changes with ambient light
    public bool _changeColor;
    // The maximum velocity of the car
    public float _maxVelocity;
    //public AIVehicle(string id, ProDMVector3 spawnPos, ProDMVector3 goalPos, Color bodyColor) : base(id, spawnPos)
    //{
    //    _testActorType = TestActorType.VEHICLE;
    //    _goalPos = goalPos;
    //    _bodyColor = bodyColor;
    //    _changeColor = false;
    //}

    //public AIVehicle(string id, RoadSegment road, float normalizedDistance, LANE laneNum, Color bodyColor) : base(id)
    //{
    //    //Get the corresponding Road & Traffic System piece and instantiate vehicle
    //    _testActorType = TestActorType.VEHICLE;

    //    _road = road;
    //    _normalizedDistance = normalizedDistance;
    //    _laneNum = laneNum;
    //    _bodyColor = bodyColor;
    //    _changeColor = false;
    //    //throw new System.Exception("This structure hasn't been fully implemented yet");
    //}

    //Body color of the car is a OneOf value
    public AIVehicle(string id, RoadSegment road, float normalizedDistance, LANE laneNum, VarEnum<Color> bodyColor, float maxVelocity = 5.5f, string model = "AICar") : base(id)
    {
        //Get the corresponding Road & Traffic System piece and instantiate vehicle
        _testActorType = TestActorType.VEHICLE;

        _road = road;
        _normalizedDistance = normalizedDistance;
        _laneNum = laneNum;

        //Choose a random body color from possibilities
        bodyColor.SetRandom();
        _bodyColor = bodyColor; // implicit OneOf conversion

        _changeColor = false;
        //throw new System.Exception("This structure hasn't been fully implemented yet");

        _maxVelocity = maxVelocity;
        _model = model;
    }

    public AIVehicle(string id, RoadSegment road, float normalizedDistance, LANE laneNum, Color bodyColor, bool changeColor, float maxVelocity = 5.5f, string model = "AICar") : base(id)
    {
        //sets whether or not to change color with ambient lighting
        _testActorType = TestActorType.VEHICLE;

        _road = road;
        _normalizedDistance = normalizedDistance;
        _laneNum = laneNum;
        _bodyColor = bodyColor;
        _changeColor = changeColor;
        _maxVelocity = maxVelocity;
        _model = model;
        //throw new System.Exception("This structure hasn't been fully implemented yet");
    }

    public AIVehicle(string id, ProDMVector3 spawnPos, Color bodyColor, string behavior = null, string monitor = null, float maxVelocity = 5.5f, string model = "AICar") : base(id, spawnPos)
    {
        _testActorType = TestActorType.VEHICLE;
        _bodyColor = bodyColor;
        _changeColor = false;
        _maxVelocity = maxVelocity;
        _model = model;

        _behavior = behavior;
        _monitor = monitor;
    }

    public AIVehicle(string id, RoadSegment road, float normalizedDistance, LANE laneNum, Color bodyColor, float maxVelocity = 5.5f, string model = "AICar", string behavior = null, string monitor = null) : base(id)
    {
        //Get the corresponding Road & Traffic System piece and instantiate vehicle
        _testActorType = TestActorType.VEHICLE;

        _road = road;
        _normalizedDistance = normalizedDistance;
        _laneNum = laneNum;
        _bodyColor = bodyColor;
        _changeColor = false;
        //throw new System.Exception("This structure hasn't been fully implemented yet");
        _maxVelocity = maxVelocity;
        _model = model;

        _behavior = behavior;
        _monitor = monitor;
    }
}