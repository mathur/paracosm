﻿/*
 * This script contains data structures for specifying actors in the test environment
 * 
 * */
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public enum TestActorType {NONE, VEHICLE, PEDESTRIAN, EGO_VEHICLE, EGO_VEHICLE_AUTONOMOUS, EGO_VEHICLE_TRAINING };

[System.Serializable]
//Test actors are actors like vehicles or pedestrians that are part of the test
public class TestActor
{
    public TestActorType _testActorType = TestActorType.NONE;

    public string _id; //Identifier for the actor 

    //Position at which the Actor is spawned
    public ProDMVector3 _spawnPos;

    //Live position of the Actor
    public List<Vector3> _livePos;

    //Live speed of the actor
    public List<float> _liveSpeed;

    public List<OnlineEvent> _events;

    public string _behavior;
    public string _monitor;

    public TestActor(string id, ProDMVector3 spawnPos)
    {
        _testActorType = TestActorType.NONE;

        _id = id;
        _spawnPos = spawnPos;

        _livePos = new List<Vector3>();
        _liveSpeed = new List<float>();
        _events = new List<OnlineEvent>();
    }

    public TestActor(string id)
    {
        _testActorType = TestActorType.NONE;

        _id = id;
        _spawnPos = Vector3.zero;

        _livePos = new List<Vector3>();
        _liveSpeed = new List<float>();
        _events = new List<OnlineEvent>();
    }

    //Method that updates position and speed of actors live
    public void UpdateLivePositionAndSpeed(Vector3 position, float speed)
    {
        _livePos.Add(position);
        _liveSpeed.Add(speed);
    }
}



