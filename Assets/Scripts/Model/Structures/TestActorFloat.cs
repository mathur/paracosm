﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

/* currently added to the AI car to make the color change when ambient light is above a certain value */

public class TestActorFloat : MonoBehaviour {
    public AIVehicle actorRef;

    void Start()
    {
        if (actorRef == null)
            throw new System.Exception("Some essential properties of TestActorFloat are null");
    }

    void Update () {
        if (RenderSettings.ambientIntensity >= 2.0)
        {
            //Debug.Log("Made it here");
            EnvironmentModel.testActorList[actorRef].GetComponent<ChangeMaterialColour>().ChangeColor(new Vector4(1, 1, 1, 1));
        }
        else
        {
            EnvironmentModel.testActorList[actorRef].GetComponent<ChangeMaterialColour>().ChangeColor(actorRef._bodyColor);
        }
    }
}
