﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBehavior
{

    public string behaviorID;
    public int count;
    public List<object> parameters;

    public OnlineBehavior()
    {
        parameters = new List<object>();
    }

    public OnlineBehavior(string n = null, int c = 0)
    {
        behaviorID = n;
        count = c;
        parameters = new List<object>();
    }

    public void updateParameters(List<object> paramList)
    {
        parameters = paramList;
    }
}
