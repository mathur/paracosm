/*
 * Consists of basic structure that is used to model more complicated road networks
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum RoadElementType {NONE, CONTIGUOUS_ROAD, INTERSECTION};

public enum Orientation {VERTICAL = 0, VERTICAL_FLIPPED = 180, HORIZONTAL = 90, HORIZONTAL_FLIPPED = 270};

public enum Zoning {NONE, TREES, RESIDENTIAL_ZONE, INDUSTRIAL_ZONE, FARM_ZONE, COMMERCIAL_ZONE};


//Describes a basic road element
public class RoadElement
{
    public RoadElementType _roadElementType;

    public string _id; //An identifier for this particular road element
    public int _idNum;

    //Parent/child information
    public RoadElement _parent; //The parent of this road element
    public List<RoadElement> _children; //Direct children of this element
    public List<RoadElement> _descendents; //List of all descendents of this element

    //Lists of connected and free connection points
    //The indices are clockwise for non-flipped segments
    public Connections _connections;

    public Orientation _orientation; //How is this particular element oriented?

    //The x,y position of the road segment.
    //Note- This does not conform to the Unity default y=up.
    //TODO: This shall be deprecated
    public VarInterval<float> _posX;
    public VarInterval<float> _posY;

    public ProDMVector3 _pos;

    public VarEnum<int> _numLanes; //num of lanes in the road element

    //Constructor for base Road Element type
    public RoadElement(string id = "")
    {
        _idNum = EnvironmentModel.globalRoadID;
        EnvironmentModel.globalRoadID = EnvironmentModel.globalRoadID + 1000;

        if (id == "")
        {
            _id = _idNum.ToString();
        }
        else
        {
            _id = id;
        }
        
        _roadElementType = RoadElementType.NONE;
        //We do not init _connections yet
        _orientation = Orientation.VERTICAL;
        _pos = Vector3.zero;

        //Make new lists for children
        _children = new List<RoadElement>();
        _descendents = new List<RoadElement>();

        //Empty Connections object
        _connections = new Connections();
        _numLanes = new VarEnum<int>(new List<int> { 1, 2, 4, 6 });

        
    }

    //Called when a list of road elements is supplied (make this RoadElement an encapsulation of the supplied collection)
    public RoadElement (List<RoadElement> collection, string id = "") : this(id)
    {
        foreach (var elem in collection)
        {
            elem.SetParentAs(this);
        }
    }

    //called to generate a road grid network
    public RoadElement buildRoadGrid (VarEnum<int> numLanes, VarInterval<int> numRows, VarInterval<int> numCols, VarInterval<int> lengthofconnection)
    {
        //we need this to connect rows to each other
        RoadElement currentRow = new RoadSegment(1, 2, nameof(currentRow));
        RoadElement previousRow = new RoadSegment(1, 2, nameof(previousRow));
        List<PhysicalConnection> currentTops = new List<PhysicalConnection>();
        List<PhysicalConnection> previousTops = new List<PhysicalConnection>();
        List<PhysicalConnection> currentBottoms = new List<PhysicalConnection>();
        //we need this to connect the elements in the row
        RoadElement previousCol = new RoadSegment(1, 2, nameof(previousCol));
        PhysicalConnection previousRight = previousCol._connections.NextFree();

        for (int row = 0; row < numRows; row++)
        {

            for (int col = 0; col < numCols; col++)
            {
                Intersection int0 = new CrossIntersection(numLanes, nameof(int0) + row.ToString() + col.ToString());
                RoadSegment cRoad0 = new RoadSegment(lengthofconnection, numLanes, nameof(cRoad0) + row.ToString() + col.ToString());
                RoadSegment cRoad3 = new RoadSegment(lengthofconnection, numLanes, nameof(cRoad3) + row.ToString() + col.ToString());

                List<RoadElement> cRoadList0 = new List<RoadElement>();
                cRoadList0.Add(cRoad0);
                cRoadList0.Add(cRoad3);

                //Make an encapsulating object
                RoadElement cRoads0 = new RoadElement(cRoadList0, nameof(cRoads0) + row.ToString() + col.ToString());

                //Connect the encapsulated object to Interesection
                Dictionary<PhysicalConnection, PhysicalConnection> connMap0 = new Dictionary<PhysicalConnection, PhysicalConnection>();
                connMap0.Add(cRoad0._connections.AtIndex(1), int0._connections.AtIndex(0));
                connMap0.Add(cRoad3._connections.AtIndex(1), int0._connections.AtIndex(3));

                RoadElement connectedElement0 = cRoads0.ConnectTo(int0, connMap0, nameof(connectedElement0) + row.ToString() + col.ToString());

                if (col != 0)
                {

                    Dictionary<PhysicalConnection, PhysicalConnection> connMap = new Dictionary<PhysicalConnection, PhysicalConnection>();
                    connMap.Add(int0._connections.AtIndex(2), previousRight);

                    RoadElement colElement = connectedElement0.ConnectTo(previousCol, connMap, nameof(colElement) + row.ToString() + col.ToString());

                    //we need this to connect the elements in the row
                    previousCol = colElement;
                    previousRight = cRoad0._connections.AtIndex(0);
                    currentTops.Add(cRoad3._connections.AtIndex(0));
                    currentBottoms.Add(int0._connections.AtIndex(1));
                }
                else
                {
                    //we need this to connect the elements in the row
                    previousCol = connectedElement0;
                    previousRight = cRoad0._connections.AtIndex(0);
                    currentTops.Add(cRoad3._connections.AtIndex(0));
                    currentBottoms.Add(int0._connections.AtIndex(1));
                }

                if (col == numCols - 1)
                {
                    currentRow = previousCol;
                }
            }

            //now we connect the current row to the previous row
            if (row != 0)
            {

                Dictionary<PhysicalConnection, PhysicalConnection> connMap1 = new Dictionary<PhysicalConnection, PhysicalConnection>();
                connMap1.Add(currentBottoms[0], previousTops[0]);

                RoadElement rowElement = currentRow.ConnectTo(previousRow, connMap1, nameof(rowElement) + row.ToString());

                //we need this to connect the elements in the row
                previousRow = rowElement;
                previousTops.Clear();
                foreach (PhysicalConnection conn in currentTops)
                {
                    previousTops.Add(conn);
                }
                currentTops.Clear();
                currentBottoms.Clear();
            }
            else
            {
                //we need this to connect the elements in the row
                previousRow = currentRow;
                previousTops.Clear();
                foreach (PhysicalConnection conn in currentTops)
                {
                    previousTops.Add(conn);
                }
                currentTops.Clear();
                currentBottoms.Clear();
            }
        }

        RoadElement finalElement = previousRow;
        finalElement._id = this._id;
        return finalElement;
    }

    //Connect r1 (this) to r2, by setting r2 as parent and making connections as defined in connectionMap
    public RoadElement ConnectTo(RoadElement r2, Dictionary<PhysicalConnection, PhysicalConnection> connectionMap, string id = "")
    {
        RoadElement r1 = this;
        //Set parenting
        r1.SetParentAs(r2); //Setting parenting at this level won't work in the general case

        if (r1._connections.MakeConnections(r1, r2, connectionMap))
        {
            RoadElement abstractObject = new RoadElement(id);
            //What happens when r2 already has a parent?
            if (r2._parent != null)
            {
                RoadElement r2Parent = r2._parent;
                abstractObject.SetParentAs(r2Parent);
            }
            r2.SetParentAs(abstractObject); //Set new object to be the parent of r2
            return abstractObject;
        }
        else return null;
    }

    //Set this RoadElement's parent to 'parent'
    public void SetParentAs(RoadElement parent)
    {
        if (parent != null && (_parent == null)) //The current parent of the RoadElement is null and the new parent is not null
        {
            //check that the prospective parent passed is not a descendent of the current object
            foreach(var descendent in _descendents)
            {
                if (descendent == parent)
                    throw new Exception("Cannot set parenting as requested as it would create a cycle (illegal).");
            }

            //Set parent data member
            _parent = parent;
            //Set children/descendent data members in the parent
            parent._children.Add(this);
            parent._descendents.Add(this); //A child is also a descendent
            parent.AddDescendentsOf(this);

            //Ensure only distinct values
            parent.MaintainDistinctChildrenAndDescendents();
        }
        else
            throw new Exception("Illegal operation. Cannot set parent in the provided parent, child pair.");
    }
    
    //Append descendents of the child to the descendents of caller.
    public void AddDescendentsOf(RoadElement child)
    {
        _descendents.AddRange(child._descendents); //Append the new list of descendents
    }

    //Check if caller encapsulates RoadElement query
    public bool Encapsulates(RoadElement query)
    {
        if (this == query) //If reference is equal to query (same object)
        {
            return true;
        }
        else //Is one of the descendents
        {
            foreach (var elem in _descendents)
                if (query == elem) //If references are equal (alternative is checking _id vals)
                    return true;
        }
        return false;
    }

    //Ensure distinct children and descendents are stored
    public void MaintainDistinctChildrenAndDescendents()
    {
        _children = _children.Distinct().ToList();
        _descendents = _descendents.Distinct().ToList();
    }

    //TODO: Validity and contiguity checks

    //TODO: Methods that calculate nice properties. For example reachability, directionality, etc.

    //Is RoadElement dest reachable from this
    public bool Reachable(RoadElement dest)
    {
        //Check on id and not reference
        if (_id == dest._id)
            return true; //trivially true
        else
            foreach (var elem in _descendents) 
            {
                //foreach descendent of this RoadElement
                if(elem._id == dest._id)
                {
                    return true;
                }
            }
        return false;
    }

}


