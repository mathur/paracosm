﻿/*
 * This is the definition of the class structure used to store relevant event information.
 * These may be events such as collision of ego car, bad driving behaviour etc.
 *  */

using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

[System.Serializable]
//Helper enumerator
public enum OnlineEventType {NONE, COLLISION, ABRUPT_BRAKE, INACTIVITY, ALMOST_COLLISION, OTHER};

[System.Serializable]
// Base class for online events
public class OnlineEvent
{
    public OnlineEventType _type;
    public float _energy; // Added 17.07.2019. The energy is used by Global Optimizers.
    public string _desc;
    
    public string _timestamp;

    public string _ownerID;
    //The Actor that owns the OnlineEvent structure
    [System.NonSerialized]
    private TestActor _owner;

    public OnlineEvent(TestActor owner)
    {
        _type = OnlineEventType.NONE;
        _desc = "NONE";
        _owner = owner;
        _ownerID = owner._id;
        _timestamp = System.DateTime.Now.ToLongTimeString();
        _energy = 0f;
    }
}
[System.Serializable]
// Class that stores collision events
public class CollisionEvent : OnlineEvent
{
    //The suspect involved in the collision (apart from the owner)
    public string _suspect;
    //Velocity of collision
    public Vector3 _collisionVelocity;
    public float _velocityMagnitude;

    public CollisionEvent(TestActor owner, string suspect, Vector3 collisionVelocity, float energy = 0f) : base(owner)
    {
        _type = OnlineEventType.COLLISION;
        _desc = "COLLISION";
        _suspect = suspect;
        _collisionVelocity = collisionVelocity;
        _velocityMagnitude = collisionVelocity.magnitude;
        _energy = energy;
    }
}

[System.Serializable]
// TODO: Not yet implemented
public class AlmostCollisionEvent : OnlineEvent
{
    //The suspect involved in the collision (apart from the owner)
    public string _suspect;
    // Minimum vector3 difference of position
    public Vector3 _minDifference;
    // minimum distance
    public float _minDifferenceMagnitude;

    public AlmostCollisionEvent(TestActor owner, string suspect, Vector3 minDifference, float minDifferenceMagnitude, float energy = 0f) : base(owner)
    {
        _type = OnlineEventType.ALMOST_COLLISION;
        _desc = "ALMOST_COLLISION";
        _suspect = suspect;
        _minDifference = minDifference;
        _minDifferenceMagnitude = minDifferenceMagnitude;
        _energy = energy;
    }
}

[System.Serializable]
//Abrupt brake events
public class AbruptBrakeEvent : OnlineEvent
{
    public AbruptBrakeEvent(TestActor owner) : base(owner)
    {
        _type = OnlineEventType.ABRUPT_BRAKE;
        _desc = "ABRUPT_BRAKE";
    }
}
[System.Serializable]
public class InactivityEvent : OnlineEvent
{
    public InactivityEvent(TestActor owner) : base(owner)
    {
        _type = OnlineEventType.INACTIVITY;
        _desc = "INACTIVITY";
    }
}
[System.Serializable]
public class OtherEvent : OnlineEvent
{
    public OtherEvent(TestActor owner, float energy) : base(owner)
    {
        _type = OnlineEventType.OTHER;
        _energy = energy;
    }
}