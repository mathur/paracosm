﻿/*This script has descriptions of basic data types useful for pro-direct manipulation.
 * Added functionality is min and max value if the object is 'changeable'.
 * This additional info is useful for specifying constraints in code and playing around in direct manipulation.
 * */

using System;
using System.Collections.Generic;
using UnityEngine;
//Reactive extensions
using UniRx;


[Serializable]
//Enum to distinguish type of enum
public enum ProDMType {NONE, INT, FLOAT, ONE_OF, COLOR, VECTOR3, VECTOR2};
public enum AutomatedVarSetMethod {RANDOM, LOW_DISCREPANCY, OPTIMIZE_ENERGY_HALTON_SAMPLE, OPTIMIZE_ENERGY_RANDOM_SAMPLE, OPTIMIZE_ENERGY_HALTON_MAX, OPTIMIZE_ENERGY_RANDOM_MAX, OPTIMIZE_BEHAVIOR };
/* These options control how the tests will be run:
RANDOM: selects random values for given number of iterations
LOW_DISCREPANCY: selects halton sequence values for a given number of iterations
OPTIMIZE_ENERGY_HALTON_SAMPLE: uses simulated annealing to determine parameters for maximum energy with halton sequence values
OPTIMIZE_ENERGY_RANDOM_SAMPLE: uses simulated annealing to determine parameters for maximum energy with random values
OPTIMIZE_ENERGY_HALTON_MAX: like OPTIMIZE_ENERGY_HALTON_SAMPLE, but does initial test of halton seq values, then performs SA on top k energies 
OPTIMIZE_ENERGY_RANDOM_MAX: like OPTIMIZE_ENERGY_RANDOM_SAMPLE, but does initial test of halton seq values, then performs SA on top k energies
OPTIMIZE_BEHAVIOR: uses fuzzing to ensure all target behaviors are covered

*/

[Serializable]
//Base class for all ProDM classes
public class ProDM
{
    public ProDMType _type = ProDMType.NONE;
    public string _desc = "NONE";

}

//[Serializable]
//public class VarInterval : ProDM
//{
//    public int _val;
//    public int _min;
//    public int _max;
//    public bool _changeable;

//    //Constructor
//    public VarInterval (int val, bool changeable = true, int min = int.MinValue, int max = int.MaxValue, string desc = "INT")
//    {
//        _type = ProDMType.INT;

//        _val = val;

//        _changeable = changeable;
//        //If it is changeable, we have min and max values
//        if (_changeable)
//        {
//            _min = min;
//            _max = max;

//        }
//        _desc = desc;
//    }

//    //change the value
//    public void ChangeValue(int val)
//    {
//        if (val >= _min && val <= _max)
//        {
//            _val = val;
//        }
//        else
//        {
//            throw new Exception("The required value cannot be set as it either exceeds _max or is less than _min");
//        }
//    }

//    //Implicit casting from/to int 
//    public static implicit operator VarInterval (int val)
//    {
//        return new VarInterval (val:val,changeable:false);
//    }

//    public static implicit operator int (VarInterval obj)
//    {
//        return obj._val;
//    }
//}

//[Serializable]
//A wrapper for float specific for pro-direct manipulation
//public class VarInterval : ProDM
//{
//    public float _val;
//    public float _min;
//    public float _max;
//    public bool _changeable;


//    //Constructor
//    public VarInterval (float val = 0f, bool changeable = true, float min = 0f, float max = 1f, string desc = "FLOAT")
//    {
//        _type = ProDMType.FLOAT;

//        _val = val;
//        _changeable = changeable;
//        //If it is changeable, we have min and max values
//        if (_changeable)
//        {
//            _min = min;
//            _max = max;
//        }

//        _desc = desc;
//    }

//    //Copy constructor
//    public VarInterval(VarInterval orig)
//    {
//        _type = orig._type;
//        _desc = orig._desc;
//        _min = orig._min;
//        _max = orig._max;
//        _val = orig._val;
//        _changeable = orig._changeable;

//    }

//    //change the value
//    public void ChangeValue(float val)
//    {
//        if (val >= _min && val <= _max)
//        {
//            _val = val;
//        }
//        else
//        {
//            throw new KeyNotFoundException("The required value cannot be set as it either exceeds _max or is less than _min");
//        }
//    }

//    //Implicit casting from/to float 
//    public static implicit operator VarInterval(float val)
//    {
//        return new VarInterval(val: val, changeable: false);
//    }

//    public static implicit operator float (VarInterval obj)
//    {
//        return obj._val;
//    }

//    //Random number generation
//    public void SetRandom()
//    {
//        _val = UnityEngine.Random.Range(_min, _max);
//    }


//    ////Additive recurrence method is used 
//    public List<float> CalculateLowDiscrepancy (int iterations)
//    {
//        List<float> lowDiscSeq  = new List<float>(iterations);
        
//        float tempRandom = UnityEngine.Random.Range(0f, 1f);
//        float irrational = (Mathf.Sqrt(5f) - 1f) / 2f;

//        for (int i =0; i < iterations; i++)
//        {
//            tempRandom = (tempRandom + irrational) % 1f;
//            float scaledValue = (tempRandom * (_max - _min)) + _min;
//            lowDiscSeq.Add(scaledValue);
//        }
        
//        return lowDiscSeq;
//    }
//}

[Serializable]
public class VarInterval<T> : ProDM where T : IComparable
{
    public T _val;
    public T _min;
    public T _max;
    public bool _changeable;

    public VarInterval(T val = default, T min = default, T max = default, bool changeable = true, string desc = "FLOAT")
    {
        if (typeof(T) == typeof(float))
        {
            _type = ProDMType.FLOAT;
            
        }
        else if (typeof(T) == typeof(int))
        {
            _type = ProDMType.INT;
            
        }
        else
        {
            throw new KeyNotFoundException("Can only use int or float values for VarInterval<T>");
        }
        

        _val = val;
        _changeable = changeable;
        //If it is changeable, we have min and max values
        if (_changeable)
        {
            _min = min;
            _max = max;
        }

        _desc = desc;
    }

    //Copy constructor
    public VarInterval(VarInterval<T> orig)
    {
        _type = orig._type;
        _desc = orig._desc;
        _min = orig._min;
        _max = orig._max;
        _val = orig._val;
        _changeable = orig._changeable;

    }

    //change the value
    public void ChangeValue(T val)
    {
        if ((dynamic)val >= (dynamic)_min && (dynamic)val <= (dynamic)_max)
        {
            _val = val;
        }
        else
        {
            throw new KeyNotFoundException("The required value cannot be set as it either exceeds _max or is less than _min");
        }
    }

    //Implicit casting from/to float 
    public static implicit operator VarInterval<T>(T val)
    {
        return new VarInterval<T>(val: val, changeable: false);
    }

    public static implicit operator T(VarInterval<T> obj)
    {
        return obj._val;
    }

    //Random number generation
    public void SetRandom()
    {
        _val = UnityEngine.Random.Range((dynamic)_min, (dynamic)_max);
    }


    ////Additive recurrence method is used 
    public List<float> CalculateLowDiscrepancy(int iterations)
    {
        List<float> lowDiscSeq = new List<float>(iterations);

        float tempRandom = UnityEngine.Random.Range(0f, 1f);
        float irrational = (Mathf.Sqrt(5f) - 1f) / 2f;

        for (int i = 0; i < iterations; i++)
        {
            tempRandom = (tempRandom + irrational) % 1f;
            float scaledValue = (tempRandom * ((dynamic)_max - (dynamic)_min)) + _min;
            lowDiscSeq.Add(scaledValue);
        }

        return lowDiscSeq;
    }
}

[Serializable]
//Class that allows saying something is one of a few given values
public class VarEnum<T> : ProDM
{
    public List<T> _valOneOf; //The value at any given point is one of these vals
    public int _index; //Current index position in the list
    public int _size;

    public VarEnum(List<T> valOneOf, int index = 0, string desc = "ONE_OF")
    {
        _type = ProDMType.ONE_OF;

        _valOneOf = new List<T>(valOneOf);
        _index = index;

        _desc = desc;

        //Number of elements in the list
        _size = valOneOf.Count;
    }

    //copy constructor
    public VarEnum (VarEnum<T> orig)
    {
        _type = ProDMType.ONE_OF;

        _valOneOf = orig._valOneOf;
        _index = orig._index;

        _desc = orig._desc;

        //Number of elements in the list
        _size = orig._valOneOf.Count;
    }

    //Changing _index properly
    public void ChangeIndex(int index)
    {
        if(index >= 0 && index<_valOneOf.Count)
        {
            _index = index;
        }
        else
        {
            throw new KeyNotFoundException("The required index cannot be set as it either exceeds bounds of _valOneOf or is less than 0");
        }
    }
    public void SetRandom()
    {
        //Sets the OneOf value to a random index
        int randIndex = UnityEngine.Random.Range(0, _size);
        ChangeIndex(randIndex);
    }

    public void SetDefault()
    {
        //Sets the OneOf value to be default, i.e., index 0
        ChangeIndex(0);   
    }

    //Smart casts to/from VarEnum
    public static implicit operator VarEnum<T> (T val)
    {
        return new VarEnum<T>(new List<T> { val }, 0);
    }

    public static implicit operator T (VarEnum<T> obj)
    {
        return ((obj._valOneOf[obj._index]));
    }

}

[Serializable]
//Some data types for specifying loctaion
public class ProDMVector2 : ProDM
{
    public VarInterval<float> _x, _y;

    public ProDMVector2(VarInterval<float> x, VarInterval<float> y)
    {
        _type = ProDMType.VECTOR2;

        _x = x;
        _y = y;
    }

    //Implicit casting from/to Unity's Vector2 
    public static implicit operator ProDMVector2(Vector2 val)
    {
        return new ProDMVector2(val.x, val.y);
    }

    public static implicit operator Vector2(ProDMVector2 obj)
    {
        Vector2 retObj = new Vector2(obj._x, obj._y);
        return retObj;
    }

}

[Serializable]
public class ProDMVector3 : ProDM
{
    public VarInterval<float> _x, _y, _z;

    public ProDMVector3(VarInterval<float> x, VarInterval<float> y, VarInterval<float> z)
    {
        _type = ProDMType.VECTOR3;

        _x = x;
        _y = y;
        _z = z;
    }

    //Implicit casting from/to Unity's Vector3
    public static implicit operator ProDMVector3(Vector3 val)
    {
        return new ProDMVector3(val.x, val.y, val.z);
    }

    public static implicit operator Vector3(ProDMVector3 obj)
    {
        Vector3 retObj = new Vector3(obj._x, obj._y, obj._z);
        return retObj;
    }
}

[Serializable]
//Specifying color r,g,b,a, normalized to [0,1]
public class ProDMColor : ProDM
{
    public VarInterval<float> _r, _g, _b, _a;

    public ProDMColor(VarInterval<float> r, VarInterval<float> g, VarInterval<float> b, VarInterval<float> a)
    {
        _type = ProDMType.COLOR;

        _r = r;
        _g = g;
        _b = b;
        _a = a;
    }

    //Implicit casting from/to Unity's Color class 
    public static implicit operator ProDMColor(Color val)
    {
        return new ProDMColor(val.r, val.g, val.b, val.a);
    }

    public static implicit operator Color(ProDMColor obj)
    {
        Color retObj = new Color(obj._r, obj._g, obj._b, obj._a);
        return retObj;
    }
}



