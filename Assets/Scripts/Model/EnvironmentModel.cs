﻿/*
 * A public static class shared across other scripts.
 * Holds active variables and visual elements.
 * TODO: Possibly call this something else?
 *  */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnvironmentModel
{
    //Dictionary of RoadElements currently in use. Also the most updated list of newly created / modified elements.
    public static Dictionary<string, RoadElement> roadElements = new Dictionary<string, RoadElement>();
    public static Dictionary<string, GameObject> roadElementGOMapping = new Dictionary<string, GameObject>();

    public static Dictionary<TestActor, GameObject> testActorList = new Dictionary<TestActor, GameObject>();

    public static Dictionary<string, ActorMonitor> actorMonitorList = new Dictionary<string, ActorMonitor>();

    //Invariants in a run
    public static List<VarInterval<float>> invariants;

    public static int globalRoadID = 1000;
}
