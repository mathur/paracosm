﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//State of each road element
public enum RoadElementState
{
    INITIALIZING,
    IN_PROGRESS,
    PLACED
};

public class RoadElemCollisionCheck : MonoBehaviour
{
    public RoadElementState state = RoadElementState.INITIALIZING;

    //A list that stores currently colliding gameobjects. 
    //As OnCollisionExit is not hit when gameobjects are destroyed, this list is helpful and is checked on every update.
    public List<GameObject> currentlyInCollision = new List<GameObject>();

	// Update is called once per frame
	void Update ()
    {
        List<GameObject> toRemove = new List<GameObject>(); //List of objects to be removed
        
        //Check if a in-collision gameobject has been destroyed
        foreach (GameObject elem in currentlyInCollision)
        {
            if (elem == null)
            {
                Debug.Log("A gameobject that was previously in collision has been destroyed");
                //Build the toDelete list
                toRemove.Add(elem);
            }
        }
        
        //Delete each item in toDelete
        foreach(GameObject elem in toRemove)
        {
            currentlyInCollision.Remove(elem);
        }
	}

    private void OnTriggerEnter(Collider collidingObj)
    {
        //Debug.Log("Entering collision with " + collidingObj.gameObject.name);
        if(!currentlyInCollision.Contains(collidingObj.gameObject)) //Avoid duplicates
            currentlyInCollision.Add(collidingObj.gameObject);
    }
    private void OnTriggerExit(Collider collidingObj)
    {
        //Debug.Log("Exiting collision with " + collidingObj.gameObject.name);
        currentlyInCollision.Remove(collidingObj.gameObject);
    }
}
